+++
template = "generic_content.html"
+++

Welcome! This page features farm-fresh jokes cultivated straight from my neurons, cruelty-free. (Though my wife,
faithful sounding board she is, has described the workshopping process as "painful.")

I encourage you to share these freely, so that the world might be more replete with laughter. But think of me every time
you tell one.

{% bubble() %}
Why did the War Amps branch have to close?\
They didn't have enough members.
{% end %}

{% bubble() %}
Knock, knock.\
Who's there?\
The World Health Organization.\
The World Health Organization WHO?\
Ah, I see you're familiar with our work&#8230;
{% end %}

{% bubble() %}
Did you hear about the French chef who misplaced his prized ingredient then killed himself?\
Yes&mdash;the police say he lost the _huile d'olive_.
{% end %}

{% bubble() %}
What do werewolves use to thicken their soups?\
_A-roux! A-a-rooooux!_\
(Alternative setup: what did [President Nixon](https://www.youtube.com/watch?v=0tEBjqDZfzo) use to thicken his soups?)
{% end %}

{% bubble() %}
Yeah, I had an appointment with that Dr. Duck once. But it turns out he was a total quack.
{% end %}

{% bubble() %}
What do you get when you put a bunch of music-playing polymers together?\
A rubber band!
{% end %}

## Seriously Stupid [Spoonerisms](https://en.wikipedia.org/wiki/Spoonerism)

{% bubble() %}
What's the difference between a look of confusion and a German badger?\
In the first, your brow furrows; the second is Frau Burrows.
{% end %}


{% bubble() %}
What's the difference between a Hefeweizen and Tiger Woods?\
The first's a wheat beer, the second beat Weir.\
_(N.B. this is best told with a ridiculously overdone German accent.)_
{% end %}
