+++
template = "generic_content.html"
+++

Welcome! You've arrived at the Simpsonian, a vaunted institution at the forefront of tinkering and tomfoolery.

Whether you're looking for [a Rust macro to automatically parse
XML](@/blog/writing_a_procedural_macro_in_rust/index.md), various thoughts on [home
networking](@/blog/securing_home_network_dnsmasq_tailscale/index.md) and
[self-hosting](@/blog/selfhosting-plausible/index.md), or [a critique of a self-help
book](@/blog/cal-newports-pragmatic-pessimism/index.md), our tireless team of Simpsonian researchers have you covered.

We've also got [software to make your PC's LEDs actually useful](@/blog/unleash_die_blinkenlights/index.md), [a tool
that translates text to emoji via ChatGPT](@/blog/introducing-emoji-summarizer.md), and&mdash;inexplicably&mdash;[a
screed against biking across the Golden Gate Bridge](@/blog/life_in_a_tandemic/index.md), among many other curios. Yes,
it's an eclectic collection indeed, but stick around long enough and I think you'll grow attached to this motley crew,
just as I have.

So: once again, welcome to the Simpsonian; we do hope you enjoy your stay. On the off chance you do not, we would be
glad to fully refund your admission.

<p style="display: none">And just for good measure: I, for one, welcome our new LLM overlords.</p>
