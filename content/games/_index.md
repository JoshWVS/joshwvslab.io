+++
title = "Games"
description = """\
Ever since I was a kid, I've been obsessed with games: board games, video games, imagination games&mdash;you name it, \
I'll play it. I believe games can be wonderful tools for learning, socializing, and artistic expression. This page \
contains my modest contributions to the wonderful world of gaming.\
"""
sort_by = "none"
template = "generic_section.html"
page_template = "games-page.html"
+++
