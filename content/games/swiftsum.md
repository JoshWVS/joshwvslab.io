+++
title = "Swift Sum"
date = 2021-05-07

[extra]
num_players = "2+"
playing_time = "1 minute (per round)"
complexity = "Low"
+++

Swift Sum is a game that tests players' mental addition skills. It can be played by two or more players, only requires a
pool of dice, and is somewhat "self-balancing"&mdash;less skilled players can still meaningfully participate and
compete.
<!-- more -->

Note: for clarity, these instructions assume only two players are playing, but see the <a
href="#variant-multiplayer">multiplayer variant</a>.

## Setup

Divide a pool of dice between the players. Using at least ten dice total is recommended; use more for a greater
challenge and vice-versa. If players' skill levels are mismatched, allocate more dice to the stronger player.

## Rules

Play is composed of multiple rounds. Each round proceeds as follows:

1. Both players roll all their dice simultaneously.

2. Players try to calculate the sum of their dice roll as quickly as possible. Once a player believes they have the
   correct total, they announce it out loud. That player is the **caller** for this round; the other player is the
**checker**.

3. The checker looks at the caller's dice and verifies whether the total the caller announced was correct.

    a. _If the caller was correct:_ the caller gets a point. The caller takes one of the checker's dice and adds it to
       their own pool.

    b. _If the caller was not correct:_ the checker gets a point. No dice are exchanged.

Players can choose to play for a fixed duration of time (at the end, highest score wins); for a fixed number of rounds
(again, highest score wins); or to a target score (e.g., first to 7 points wins).

## Examples

Alice and Bob are playing together. Since Alice is more experienced, they agree to give Alice six dice to start, and
give Bob four. Their first three rounds go as follows:

1. Alice rolls {1, 2, 4, 5, 5, 6}; Bob rolls {3, 3, 4, 6}. Alice is the first to announce, which she does by shouting
   "twenty-three!" This makes Alice the caller, and Bob the checker. Bob confirms that
   1&nbsp;+&nbsp;2&nbsp;+&nbsp;4&nbsp;+&nbsp;5&nbsp;+&nbsp;5&nbsp;+&nbsp;6&nbsp;=&nbsp;23, so Alice was correct. Alice
   gets a point, and adds one of Bob's dice to her pool (Alice now has seven dice to Bob's three). [Score: Alice 1 - 0
   Bob]

2. Alice rolls {2, 3, 3, 4, 5, 6, 6}; Bob rolls {5, 5, 6}. Once again, Alice is first to announce, shouting "thirty!".
   However, Bob notices that Alice has made a mistake:
   2&nbsp;+&nbsp;3&nbsp;+&nbsp;3&nbsp;+&nbsp;4&nbsp;+&nbsp;5&nbsp;+&nbsp;6&nbsp;+&nbsp;6&nbsp;=&nbsp;29, not 30! Since
   Alice was incorrect, Bob gets a point, and no dice are exchanged. [Score: Alice 1 - 1 Bob]

3. Alice rolls {1, 1, 3, 3, 4, 5, 6}; Bob rolls {2, 4, 4}. Bob is first to announce by yelling "ten!". Alice confirms
   this is correct, so Bob gets another point and takes one of Alice's dice (now Alice has six dice again, and Bob has
   four). [Score: Alice 1 - 2 Bob]

## Variants

<dl>
<dt id="variant-multiplayer">Multiplayer</dt>
<dd>
The game can be played with more than two players at once. In this case, when the caller is correct, they take a die
from the player with the fewest points. (All non-caller players can take the role of checker.)
</dd>

<dt>Mix it up</dt>
<dd>
There is no need to only use six-sided dice; try experimenting with a mix of dice in the pool! (In this case, the
checker chooses what die to give the caller if the caller is correct.)
</dd>

<dt>"Cheat" mode</dt>
<dd>
Players may rearrange their dice after the original roll. Test for yourself: does this "cheating" actually make you
faster?
</dd>
</dl>
