+++
title = "You must visit: <em>The Under Presents</em>"
date = 2020-08-30
+++

[_The Under Presents_](https://tenderclaws.com/theunderpresents) isn't just a game: it's a magical&mdash;sometimes
inscrutable&mdash;world, and a hub for Tender Claws' delightful VR storytelling. It's a living, breathing environment
with mysteries to piece together, adventures to conquer, and spells to master. At a time when social media bubbles are
causing polarization, the absolute anonymity of The Under leads to delightful, if fleeting, moments of genuine human
connection.

<!-- more -->

Okay, clearly I've had a good time in The Under, but what _is_ it exactly? Well... it's a little complicated. The Under
proper is a nightclub at the nexus of time, and acts as a persistent multiplayer hub where you can hang out with fellow
time spirits, slinging spells. Importantly, you are all anonymous in this world; there are no usernames, friend lists,
or even voice chat. From The Under, you can also access two other experiences: _Tempest_, a live, interactive play, and
_Timeboat_, a single player time-traveling tale (we'll revisit these soon). All three of these provide background
details that fill in the lore of the universe. I expect (and hope!) that other experiences will be added over
time&mdash;The Under is your way in.

If you're a fan of interactive storytelling, and aren't afraid of having to navigate some things by yourself, I
whole-heartedly recommend diving into the deliciously different world of The Under. The introduction section below gives
some spoiler-free details on the different experiences available. I recommend starting with a ticket to _Tempest_ (the
live play, which runs to the end of September 2020) and hanging out in The Under itself a bit. If that excites you,
check out _Timeboat_ (the single player story) as well, and perhaps spend a little more time in The Under: there is more
here than meets the eye. Mysteries and magic abound, and you'd be remiss to check out early.

For those of you that want a little more detail, consider this your guided tour. I'll avoid any overt spoilers, but I will
talk in some detail about the different modes and what they contain, which wasn't obvious to me at first. We'll cover:
1. How to get started;
1. **Tempest:** an interactive, multiplayer, live VR adaptation of the Bard's tumultuous tale;
1. **Timeboat:** a single-player story of a sea voyage gone wrong (noticing a theme?), where you are a voyeuristic time
   traveller, and;
1. **The Under** itself: the live multiplayer "club" that, unexpectedly, is the real star of the bunch.

## Getting your fingers wet

On your first visit to The Under, it isn't exactly clear what the different experiences are, or how they fit together.
Here's a 10,000 foot view to get you oriented.

- Whenever you start the game, you'll begin in black-and-white purgatory. Snap your fingers and pull the glowing rings
  towards you until you have visible hands. Then, stride to the rock spires ahead of you, and put on your mask.

- With your mask on, you'll be teleported to a hub area. From there you have two options:

    1. Head towards the theatre to purchase tickets for _Tempest_ (from our beloved attendant, Russell).

    2. Head to The Under, the multiplayer nightclub, by entering the door through the building to the right. On your first
       visit you'll go through a single-player section guided by the enigmatic MC, the club's&mdash;ahem&mdash;emcee.
       Within The Under, you can access the single player _Timeboat_ experience (USD 11.99) from any of the photobooths. (You
       can also buy _Tempest_ tickets in here; Russell is very accomodating.)

If you're still confused by all this, you're probably not alone: Tender Claws themselves have a
[walkthrough](https://tenderclaws.com/tupwalkthrough) on how to get through this quasi-tutorial.

## Tempest
This is the experience that first pulled me into the world of The Under. You and up to eight other participants will act
as "spirits" assisting a live actor in putting on an adaptation of Shakespeare's _The Tempest_ (uh, spoiler warning for
_The Tempest_, I guess). Practically, this means that you participate in brief vignettes of some of the play's most
memorable moments; having some familiarity with the plot really adds to the enjoyment. At different times participating
could mean: acting out parts yourself (with direction from your supervising actor), interacting with the environment, or
just taking in some of the more fantastic effects.

Crucially, you are *completely anonymous:* there's no username floating above your head, and no voice chat to
communicate with other players. Ordinarily, this would be very limiting, but we get to appeal to some of my favourite VR
magic: body language! It really is incredible how much you can convey with finger snapping, wild gesticulating, and the
odd head tilt. That anonymity is vital in one other way&mdash;it gives participants the cover to participate
enthusiastically. Nobody is going to mock your acting after the fact, nor can your avatar really be tied to you in any
way, so why not give it your all?

The overall execution is somewhat uneven, but for good reason. You aren't viewing a theatrical performance so much as
are you involved in the production... with a total group of strangers. The end result is only as good as your crew is
enthusiastic. My performance spanned the gamut. At times, our actor felt more like a kindergarten teacher, corralling
uncooperative students along; other times, everyone seemed quite invested in our story.

An interesting quirk is that you have full use of your magic available to you during the performance. (Oh yes, there's
magic&mdash;we'll get to that.) One notable aggravation here is that casting spells creates sound effects which are
heard by everyone. The actor's audio balance is somewhat spotty to begin with (very quiet even at modest distances);
coupled with various snapping/conjuring, it can be hard to make out much of anything at times. The spells aren't always
annoying though: a highlight of my experience was when I, as [Ceres](https://en.wikipedia.org/wiki/Ceres_(mythology)),
went to bless Miranda and Ferdinand's union. Before I strode down the aisle, someone hastily conjured a fruit tart, and
embiggened it to comical proportions&mdash;a fitting tribute to the goddess of agriculture.

Overall, _Tempest_ is a touching experience. Yes, the idea is novel, the delivery is pretty good, and some of the visual
effects are captivating, but the masterpiece is the paradox at the play's beating heart: in a time when we are
physically isolated and ideologically divided, _Tempest_ strips us of our identity and reduces us to a mask. In so
doing, it creates the most intimate human contact I've had outside of my "bubble" in months. As we all know too well,
internet anonymity often leads to trolling and abuse; here it is the gentle nudge pushing you out of your comfort zone
to kiss a stranger in VR. If you have any interest in interactive storytelling, this is a must-try experience. Tickets
(USD 14.99) are available until the end of September, and also grant full access to The Under.

_(Foolishly, I forgot to take any screenshots of my experience, but you can see some on Tender Claws'
[website](https://tenderclaws.com/tempest).)_

## Timeboat
**Note:** This section will avoid any overt plot spoilers, but it will discuss some game mechanics that might not be
obvious on your first go around. Personally, I do not recommend you go in completely blind (for the reasons below), but
if you'd like to, here's your chance.

_Timeboat_ is a single player, narrative-driven experience where you watch as a sea-faring expedition... doesn't quite
go as planned. The interactions and conversations on this ill-fated voyage happen on a fixed schedule, whether or not
you are there for them. Fortunately, you can bend time to your will, and thus skip forwards/backwards as you like to
make sure you don't miss a thing. You can also switch to a cross-sectional, third-person view of the ship at any time,
easily allowing you to follow the crew members' movements. With those abilities combined, you'll quickly be able to
scamper around the ship and observe the tragedy unfold.

There's something quite unique about this structure that I feel lets you dive in deeper than you might be able to in a
more conventional format. In traditional print/video media, the plot is generally "linearized:" even in a twisty-turvy
time travelling romp, the audience can only be in one place at a time, and so you have to cut from one scene to the
next. In _Timeboat_, the story just happens, and it's up to the player to stitch the plot's individual threads together.
The best metaphor I have is xkcd's "[narrative charts](https://xkcd.com/657/):" those particular examples were created
by revisiting the story after the fact, and inferring which events overlap. In _Timeboat_, you're weaving those lines in
real-time as you observe and rewind again and again. Once you've got a grasp of what's going on, the payoff is
phenomenal: you really _do_ feel like a Time God, sneering with dramatic irony as you watch your unwitting marionettes
retrod their predestined steps.

![On the bridge with the captain in Timeboat](timeboat_bridge.jpg)

That reward loop is at its best in the first act. The scope seems overwhelming at first (the ship is so big! there are
so many people to track!), but once you're in the rhythm of things, navigation becomes second-hand, and you'll soon
learn every nook and cranny. Seemingly unrelated comments snap together in wonderful "a-ha!" moments, and a delectably
weird twist near the end sets up the rising action.

Once we get into act two, however, there are some pacing issues. You see, this journey is treacherous, and not everyone
will survive. At the intro screen before each act, you're presented with mini portraits of the crew; a dashed border
around a character means that they are about to perish&mdash;unless you intervene to save them, generally by solving a
dream-like puzzle sequence. These puzzles are dripping with possibility: at their best, they completely capture the
associated character's essence, giving you deeper insights into the their background, psyche, and how they tie in to the
broader plot. The effect here is amplified by the custom-tailored environments for each section. They don't quite rise
to the fantastical dreamscapes of _Psychonauts_, but the feeling is the same, and getting to wander through them in VR
adds a level of immersion.

Unfortunately, the puzzles themselves are... well, a total slog. In what I assume is a commitment to the narrative
experience, they offer no tutorial and no feedback. It's here where _Timeboat_ commits a cardinal sin: fail to crack the
case in time and it's game over. You'll come to in a gift shop in the middle of the The Under (the multiplayer section).
The photobooth to re-enter _Timeboat_ isn't far away, but loading up the correct act again and navigating to exactly
where you were before is tedious, and ends up discouraging exploration. It's all the more baffling from a game where the
central conceit is that you can bend time to your will: if that's the case, why pester me with game overs?

There was one occasion where I actually was on the right path, doing the right things, but missed an
important&mdash;albeit not totally obvious&mdash;detail (leading to a game over) and assumed that that section was
purely atmospheric. When I figured out my mistake and retraced my steps, I thought an encounter in another
low-visibility area was intended to feature some light combat, and wasted time trying to slay the creature I was
confronting. As it turns out, I was intended to just wander a little longer to solve this particular puzzle (no
shovel-bonking required). At first, this section was tense and nerve-wracking, but by the time I figured out how to
progress, the relief of tension wasn't "thank God I made it," it was "finally, let's get back to the story."

My advice is to play through the game at your own pace, without worrying about finding and solving the puzzles as you
go. After you've experienced the gist of the story, you can go back and try for a better outcome; but if you find
something frustrating, I wouldn't hesitate to look up the solution. (The best reference I've found is [this
comprehensive video guide](https://www.youtube.com/watch?v=k1wrcdv0mRo).) There was more than one occasion where I had
in fact stumbled on the solution, but wasn't doing it quite right&mdash;the game won't tell you this is the case, so you
can end up banging your head against the wall for a long time.

Overall, I have mixed feelings about _Timeboat_. The highs are high, and the additional exposition it gives about the
background of The Under is fascinating, but the arbitrary and capricious puzzles left me seasick at times. I would start
with _Tempest_, or just exploring The Under: if you're captivated by this world, and the possibilities of VR
storytelling, this trip is definitely worth the price of admission&mdash;just be wary that it isn't always smooth
sailing.

## The Under
Ah, The Under itself. You'd be forgiven if, like me, you assumed that this multiplayer mode would be a tacked-on side
excursion. In reality, it is anything but: The Under is best thought of as the real hub, the central world from which
all the other experiences stem (hence the moniker _The Under Presents_, presumably).

The club's main stage (near where you spawn) is a natural hangout spot and features a rotating cast of scripted acts
from various NPCs. There are a few duds among them, but those are far outweighed by the genuinely funny routines and
surprising number of earworms that I find myself humming long after I've logged off.

![Just outside The Under](the_under.jpg)

Those scripted acts are fine, but they're not all. Until the end of September 2020, The Under also features spontaneous
appearances by live actors. In my experience, they've been as varied as they are entertaining. Some examples:

- A thirsty skeleton who, after receiving a beer conjured by a fellow spirit, taught us how to make brooms;
- Pop star Helvetica Perpetua throwing a birthday party for one of my fellow time spirits;
- A disembodied voice spawning props representing chores for us (vegetables to eat, messes to tidy, books to study),
  followed by a torrent of toys afterward as a reward;
- A chill, mute old man who would vibe on the dance floor during acts, and double as an impromptu barkeep between them;
- And the most involved of the bunch: a giant "crob" (they're not "crabs" in The Under), who took us on an
  installment of her magical book tour: the improvised story she told was guided by the various items conjured by the
  players!

These encounters are delightful and unique, but they were designed to be a special addition to The Under, not the main
attraction. There's plenty of fun to be had just hanging out with passers-by, like impromptu sparring with golden swords
on the catwalk above the stage. There's also some adventures lurking in this seemingly barren desert... if you see
someone with an interesting mask, try asking how they got it.

Incidentally, I should mention that the games' principal community seems to be "The Golden Salt Shaker"
[Discord](https://discord.gg/ZdbqWC8) (no points for guessing how to identify a member in-game). Seems like a lovely
bunch of people, but just like _Tempest_, I think The Under is at its best when fully anonymous: overcoming the lack of
traditional communication options makes those moments of cooperation and insight all the sweeter.

### Magic: an introduction
So: it's about time we had a word about magic. I've somewhat glossed over it so far, but fluency in these dark arts is a
big part of The Under, so it's worth studying up. For me, the fun here is in the experimentation; there's a very logical
structure underpinning the spells, but it isn't obvious how to get started. If you want a quick primer, here are the
absolute basics:

- By taking off your mask and snapping your fingers (so that blue flames appear), your mask becomes a cauldron of sorts;
  nearly all spells begin this way.

- From what I can tell, all magic boils down to sequences of these three steps: adding an ingredient to the flames,
  snapping your fingers, or tracing a rotation around the edge of your mask (direction matters!).

- Here's the simplest possible spell: remove your mask, conjure the flames, and snap three times to produce... an
  onion!

This is another example of VR elevating a somewhat mundane mechanism to something brilliant. If ever you fantasized
about attending Hogwarts, _oh boy_, this is going to excite you. I've never been able to keep up in fighting games: long
strings of button-presses elude my fingertips and interest. But by making the gestures _physical_, this feels less like
video game trivia and more like building a life skill. That feeling is further amplified by the social nature of The
Under: it's very rewarding to share your hard-earned expertise with a fledgling time spirit. With many twists to
discover, the magic system truly captures the conspiracies, frustrations, and exultations of being a wizard. I imagine.

## Final thoughts

I was expecting to enjoy _Tempest_ as a novelty, particularly in these isolating times, and I did. What I wasn't
expecting was how _alive_ the whole world of The Under feels. It truly creates an environment larger than the player,
and demands observation, experimentation, and maybe a helping hand if you are to peel back the many layers of this
enigmatic onion. I have a feeling that I will be dropping by The Under long after the live performers have
gone&mdash;if only to share the arcane arts with the next generation.

(P.S. I haven't checked out [Virtual Virtual Reality](https://tenderclaws.com/vvr), the other Tender Claws game in the
Oculus store, but it is certainly now on my list.)
