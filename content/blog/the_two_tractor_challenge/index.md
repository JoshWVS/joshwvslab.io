+++
title = "The Two Tractor Challenge"
date = 2021-10-19
+++

When I fly to the States, I usually fly [Porter](https://www.flyporter.com), because they're often cheapest, and I'm a cheap-ass. There are two things
you should know about Porter:

1. Every flight to the U.S. will involve a connection in Toronto (YTZ);
2. Every flight serves a free snack and drink&mdash;alcoholic options included.

Furthermore, those refreshments are sourced from Canadian companies; the current beer partner is [Beau's](https://beaus.ca/), immediately recognizable
from their iconic tractor logo. With these facts in mind, allow me to introduce the _Two Tractor Challenge_ and provide the inaugural challenge
report.

{% bubble() %}
**The Two Tractor Challenge**: drink a (Beau's) beer on both legs of your Porter flight.
{% end %}

## Accepting the challenge

My brother and I recently attempted the challenge on our way back to Ottawa from New York City. This was our experience.

### EWR &#8594; YTZ

Look, there's not much to say here. We took off in full afternoon sunshine and were soon cruising above rolling clouds; why not sip a Lug Tread and
socialize with your seatmate? Life is good.

### YTZ &#8594; YOW

Here's where things start to get a little more challenging. First, you'll need to clear Canadian customs and go through security again at YTZ. Now
(hopefully) you're not anywhere near _wasted_ from a single beer, but you might be a little chatty&mdash;not a quality highly valued when performing
the requisite airport liturgy. Keep the _bon mots_ to yourself, spare the agents your barley breath, and you'll soon be on your way.

By the time we boarded the connection in YTZ, it had been a while since that first beer, and I was thirsty for the second. Now, if you simply order "a
beer" on the flight, you'll be handed a can of Lug Tread, no questions asked. That's a fine option, but allow me to fill you in on a secret: Beau's
has developed a new beer, specifically for the Porter partnership. And because sometimes, _sometimes_ this universe is just and right, they had the
common sense to brew a porter, and name it the [Porter porter](https://web.archive.org/web/20211012141730/https://beaus.ca/beer/porter-porter/)
(stylized on the can as "porter PORTER," because What Are Capitals Anyways) for Porter airlines. My heartfelt appreciation goes out to all those
involved in bringing this chocolatey, roasty, vanilla goodness into existence (kudos in particular for the recommended food pairings).

With my Porter porter in hand, coasting at a cool 600km/h, I'm feeling pretty good about myself. But this is no time to let your guard down: YTZ
&#8594; YOW is a short flight&mdash;with good weather, we could be wheels-up, wheels-down in 45 minutes. Let's be honest: there is absolutely no need
to offer food service on this flight, but by God that refined trash panda simply won't take no for an answer. The dark sweetness delights at the first
sip, but curdles cloyingly as I pound away. Reaching deep inside, I develop a tactical stealth belching regime in a desperate attempt to equalize the
pressure in my stomach&mdash;30,000 feet in the air and being filled aggressively with carbonation.

In that final sip, I no longer taste the beer itself, only the satisfaction of my impending victory sparkling on my taste buds, tinged with just a
touch of acrid bile to remind of the price greatness demands. We had done it&mdash; the inaugural Two Tractor Challenge was complete!

But, as they say, pride cometh before a fall.

A particularly obliging (or sadistic?) flight attendant had spied our antics. (I presume our matching flannel shirts added to our conspicuousness.)
Without any intervention from us, she arrived at row 15 with a fresh round of Porter porters, already cracked open and fizzing out the top.

"I brought you guys a present," she said.

We smiled, terror in our eyes.

What follows is a blur. What is one to do? Is it a crime to bring a half-finished beer off an airplane? Do they confiscate it on the way out, the
ultimate mark of failure and shame? We had no desire to find out. Gullets guzzling, diaphragms ballooning to power explosive eructations, we pressed
on, deep, deep, deeper into the inky brown depths&hellip;

The pilot announced that it was 8&#x2103; upon our arrival in Ottawa, but standing outside by arrivals, we felt no cold. Our blood ran hot with
booze and boastfulness. We had done it: the Two&mdash;no, the Three Tractor Challenge.

![Me and lil' bro completing the Three Tractor Challenge](three_tractor_challenge_complete.jpg)

Cheers.
