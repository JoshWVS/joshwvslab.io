+++
title = "Review: <em>Later Alligator</em>"
date = 2021-04-02
+++

If you're charmed by the [trailer](https://www.youtube.com/watch?v=TsPRK03ByaU)
<br>
and no minigame hater
<br>
Then I think you should play
<br>
[Later Alligator](https://www.lateralligatorgame.com/)

_(ominous mandolin strumming)_
<!-- more -->

<br>
Ah, Alligator New York City&mdash;the sights, the smells, the slime, so much slime, and home of Pat the Alligator. Pat's
a loveable, if jumpy, birthday boy in need of help. You see, Pat's convinced that "the event" his family has planned for
him tonight is a trap that will end with him being "rubbed out." Terrified, and with no one left to turn to, his special
day seems doomed to be an exercise in trepidation&mdash;that is, until he encounters a certain pinstripe-suit-wearing
fellow in the lobby. Yes, only you, THE PLAYER, have the power to investigate Pat's family and uncover what's afoot.
Hmm... though Pat's tears are pitiable (unlike crocodiles, alligators are completely genuine in that regard),
immediately taking up the cause of a total stranger is rather dramatic... unless there are other motives at play?

![Pat, on the universal categorization of footwear](shoe_kinds.jpg)

The basic gameplay of _Later Alligator_ is essentially a stripped-down Professor Layton, with less focus on pure
puzzling and a double serving of cutesy wholesome humour. You wander around the city, meeting Pat's friends & family,
and extracting very basic information about "the event" from them by helping with a task (aka minigame). But make it
quick, because time is ticking&mdash;the event starts at 8pm sharp, and you wouldn't want to be late...

The animation and music are spectacular. Having never lived in NYC, I can't vouch for "authenticity," but ANYC certainly
feels like a city _alive_, always bustling and full of quirky characters. Those characters are chock-full of distinct
personality, though everyone shares a propensity to inject a quip into every possible comment. I do think that at times
the humour would be better served with a breath between beats, but the unending tsunami of silliness is undeniably part
of the charm. The overall plot is also perfectly serviceable; mostly as window-dressing for the individual encounters,
but with a few interesting twists thrown in.

Okay, but what of the minigames themselves? Unfortunately, these challenges are like wild meat: the gamey-er they are,
the less appetizing.

The heart and soul of _Later Alligator_ is the twee art and animation. Thus, the best minigames are unobtrusive, letting
the visuals take centre stage. The absolute highlight of these is a "spot the differences" game&mdash;obviously there
isn't any "depth" to the gameplay here, but there doesn't _need_ to be, because drooling over an alligator-rethemed _[A
Sunday for the Grande Jawed](https://www.artic.edu/artworks/27992/a-sunday-on-la-grande-jatte-1884)_ (because alligators
have large jaws? ...don't blame SmallBu; this one's all me) is already hilarious and fun. Other standout examples
include an entirely unexpected dating sim, a rather realistic round of hide-and-go-seek, and an exorcism via cell phone
repair.

![A Sunday for the Grande Jawed](a_sunday_for_the_grande_jawed.jpg)

Conversely, the worst offenders squander those lovely illustrations on uninspired gameplay. Seriously, these are some
_lovely_ drawings! I want to absorb every single detail with my eye holes! But when I'm playing a Flappy Bird clone,
it's a) not that interesting in the first place, and b) makes it hard to appreciate those little details. My least
favourite example has you swatting flies so that a would-be guru can ascend to the astral plane uninterrupted. I found
this one to be downright frustrating to begin with, and unlike other minigames, failure here is game over&mdash;no
second chances. All things considered, this is a minor irk, but given how good the best minigames are, the ones at the
other end of the spectrum feel like missed opportunities.

As an aside, I was surprised to recognize many of creators involved in _Later Alligator_. SmallBu Animation are the main
creative drivers here, the two-person, husband-and-wife team that brought you [Baman
Piderman](https://www.youtube.com/watch?v=F7-TQdN40Dk&list=PLD0750A49525188E9&index=1)&mdash;but also YouTube darling
[Worthikids](https://www.youtube.com/user/Worthikids) did some clean-up work on the animation? I hadn't previously heard
of [2 Mello](http://2mello.net/home), who did a great job with the soundtrack, but [Neil Cicierega](http://neilcic.com/)
([The Ultimate Showdown of Ultimate Destiny](https://www.albinoblacksheep.com/flash/showdown), [Potter Puppet
Pals](https://www.albinoblacksheep.com/video/potterpuppetpals/)) contributed in some capacity as well? Reading up on all
the folks involved here sent me right back to my [Albino Blacksheep](https://www.albinoblacksheep.com/) days. I look
forward to seeing what they have in store next.

One other small detail that I love&mdash;the [GeoCities throwback](http://alligator.fun/) is far from original at this
point, but the fact that you can find the website both in-game and on the regular ol' internet adds some pleasing
verisimilitude. (Also, dancing alligator baby is not to be missed.)

So, should you visit Alligator New York City for yourself? I think the easiest litmus test is just to watch the trailer:
if the smooth animations, jazzy tunes, and rapid-fire one-liners elicit an "Oh, jeez!" from your cute snoot, you'll have
a lovely trip. If, however, you demand more gameplay in your games or have an aversion to chuckling, there's no need to
make the trek from Alligator New Jersey. My only real complaint here is a lament of what could have been: the visuals
and music are top-notch, but the actual minigames are often lacking in creativity and end up feeling stapled on. If
SmallBu and 2 Mello had a partner that brought equal insight to the game design aspect, and included that as a
first class consideration&mdash;now _that_ would be something special.

<br>
So ends my review
<br>
which I hope you enjoyed
<br>
for if you did not
<br>
I'll be unemployed

_(mandolin outro)_
