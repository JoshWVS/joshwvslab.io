+++
title = "Special edition travel blog: Springfield, Illinois!"
date = 2021-05-08
+++

<aside><p>
Of course, I didn't go on this trip alone&mdash;you'll find my wife's thoughts in sidebars like this one.
</p></aside>

Dear reader: usually this blog is dedicated to niche video game commentary, technical notes-to-self, and/or other
assorted nonsense, but we break from our usual programming today to provide something different. Earlier this week, I
took a short trip to Springfield, Illinois with my wife. We do rather less traveling than we ought to, and for some
reason I find my memory for special excursions like this is somewhat lacking. (Unlike punchlines to decades-old Futurama
gags, which are etched indelibly in my hippocampus).  So, as an experiment in better preserving these memories for
myself and others, we bring you this _Special Edition Travel Blog_. I hope that you'll enjoy these
roughly-chronological, overly informative highlights of our visit to the heart of the "land of Lincoln."

## Day 1

### Arrival & Head West Sub Stop
We caught the train from Chicago's [Union Station](https://chicagounionstation.com/) and _wow_, I was not expecting that
level of splendour (especially in contrast with the somewhat dated Amtrak station below). A few hours later we rolled
into Springfield, greeted by rain. Undeterred, we dropped off some bags at the inn and headed east, to [Head West Sub
Stop](https://www.headwestsubs.com/) (at a friend's recommendation&mdash;thanks!).

![The (gorgeous!) interior of Chicago's Union Station](union_station.jpg)


Those that know me are likely familiar with my weakness for sandwiches. A sandwich is a world of possibilities: hot or
cold; effectively limitless combinations of toppings and flavours; all domiciled in a homely yet portable
roll&mdash;what's not to love?

So it doesn't take much for me to love a sub shop, but I fell especially hard for Head West. Excellent veggie options
and their unabashed commitment to a nearly fluorescent green-purple-yellow colour scheme&mdash;seriously, they'd fit
right in next to a Chuck E. Cheese&mdash;combine for a hippie aesthetic I can totally get behind. The subs themselves
are reasonably priced, so naturally there was money left in the budget for one of their t-shirts&#8230;

![The interior of Head West Sub Stop](head_west.jpg)

### Dana&ndash;Thomas House
The main attraction on our first day was a guided tour of the [Dana&ndash;Thomas
House](https://www2.illinois.gov/dnrhistoric/Experience/Sites/Central/Pages/Dana-Thomas-House.aspx).  In 1902, Susan
Dana (a wealthy heiress) hired Frank Lloyd Wright to completely remodel and extend her family home&mdash;providing him
an unlimited budget and nearly full creative control to do so. (It was later purchased by a certain Charles C. Thomas
and thereafter sold to the state, hence the "Dana&ndash;Thomas" moniker.)

![The entrance of the Dana&ndash;Thomas House](dana_thomas_entrance.jpg)

Even as a complete architectural ignoramus, there was something deeply pleasing about touring the house. The singularity
of the vision and execution means that you are completely surrounded by precise lines and repeated geometrical motifs;
from the window designs, to the furniture, and indeed the architecture of the house itself (all designed by Wright!).
Free of the essential clutter of day-to-day life, the house becomes a dedicated art exhibit unto itself; an immersive
experience on a wholly unique scale.

The tour also offered insights into Dana's personal life. As an only child, she inherited her parents' vast fortune, but
loss followed her at every corner. Most notably, her only two children passed away in their infancy. An unworn
Christening gown purchased on a trip to France speaks to the hole this seems to have left in her life. At the risk of
practicing armchair psychology, it seems quite natural (and rather tragic) to connect Dana's grief to her house:
deprived of the close family relationships she desired, she filled her life hosting elaborate gatherings spanning
high-society soirées to book readings for children on Saturday mornings (capped off with ice cream from a hidden library
icebox, to boot).

I am immensely grateful that the house has been preserved and is actively being exhibited. It took an unusual set of
circumstances for it to have been built, and it would be a shame to lose such a fascinating structure to the
vicissitudes of time.

One final fun fact: the house contains a full bowling alley in the basement! While the guide will tell you it was a
favourite of Dana's, I'm of the opinion that the long, horizontal lines made it the only recreational installation
Wright would tolerate...

_(Sources: our tour guide, and the [educational
resources](https://www2.illinois.gov/dnrhistoric/Experience/Sites/Central/Pages/Dana-Thomas-House-Education.aspx) from
the Illinois Historic Preservation Division; in particular their [History of the Dana&ndash;Thomas
House](https://web.archive.org/web/20200802224347/https://www2.illinois.gov/dnrhistoric/Experience/Sites/Central/Documents/HistoryofDanaHouse.pdf))_

### Lincoln's Neighbourhood

![The Lincoln family home](lincoln_home.jpg)

<aside><p>
Although you're there for the yellow house on the corner, there is a quiet serenity to Lincoln's neighborhood that keeps
you within the block after you've taken your selfies. We had the good fortune of having the streets to ourselves as we
crunched along the gravel road and felt the creaking of the wood plank sidewalk underfoot. Within the shelter of this
pocket, I felt calm as we looked at the old homes of Lincoln's contemporaries. If even one other set of tourists had
been there, the spell would have been broken and it just would've been cool to see Lincoln's house.
</p></aside>

We wrapped up the evening with a jaunt through [Lincoln's Neighbourhood](https://www.nps.gov/liho/index.htm), a
historical recreation of several blocks of Springfield as it would have appeared in the 1860s. The main draw is of
course the former house of Honest Abe himself. It certainly must have been a place of significance for their family: it
was the only home they ever owned, three of their children were born there, and it is also where one of those children
passed away. It's a neat area in which to stroll around, but to me there isn't a huge draw here unless you're deeply
interested in the minutia of Lincoln's former neighbours.

## Day 2

### Lincoln Presidential Museum

![Outside the Abraham Lincoln Presidential Museum](lincoln_museum_entrance.jpg)

<aside><p>
The Lincoln Museum did something that no museum I've been to before managed. It moved me. We started our visit off with
the most incredible stage show I've seen. It had holographic effects that were nothing short of magical. It wasn't the
effects that got me misty eyed, but the passionate narration describing the wonder of history. The narrator mentions
what it might be like to open a box that had been sealed for a hundred years and find a lost letter of Mary Todd
Lincoln. It doesn't sound that moving, but to a history buff like me, it was inspirational. With this as my
introduction, my emotions were already heightened as we entered into the life of Lincoln. Told chronologically through
impressively immersive dioramas, the museum clearly lays out the need-to-know of Abe's life. I learned a lot of facts,
but the museum excels in its imparting of feeling. After ninety minutes of feeling the chaos of kids in your law office,
the pressure of critics, the weight of the Emancipation Proclamation, the inspiration of the Gettysburg Address, and the
sorrow of the Civil War, I genuinely cared about Abraham Lincoln more than I expected. My heart broke as I looked at
Mary grasping Abe's arm in their theatre box, a look of optimism in their eyes not felt since long before the Civil War,
while Booth stood outside drawing his gun. Moving into the next room recreating Lincoln's final funerary stop, I had to
consciously stop myself from crying. It's not like I didn't care about Lincoln before this, but I wasn't expecting to be
so moved by his life to the point where I was mourning his death. I'm sure my husband went on about the production value
of the museum, which is incredible, but for me the museum is to be commended for how it made you feel the facts of
Lincoln's life, not just know them.

</p></aside>

After a cup o' joe & delicious muffin at [Custom Cup Coffee](https://www.customcupcoffee.com/), we set off for what is
perhaps Springfield's #1 attraction: the [Abraham Lincoln Presidential Library and
Museum](https://presidentlincoln.illinois.gov/). I had already been briefly once (on the way back from a road trip to
St. Louis) and was eager to revisit it with my wife. I was as impressed this time as I was before.

The museum is basically "Abraham Lincoln's Disneyland." The production quality is sky-high; lifelike wax figurines,
historical artifacts and multimedia exhibits are used to tell the story of America's sixteenth president. I'm most
impressed by the _Ghosts of the Library_ live theatrical presentation, which blends regular stage props with special
effects in an incredibly realistic way; I haven't seen anything like this elsewhere. (It's apparently powered by
proprietary "Holavision®" technology from [BRC Imagination Arts](https://www.brcweb.com/).)

I think the museum's greatest strength is that it emphasizes the emotional story of Lincoln's life over a dry recitation
of facts. That's not to say the museum lacks in information, but it recognizes that the best way to convey the enormity
of the struggles that Lincoln faced&mdash;both as president and as a man&mdash;is to put the visitor in his shoes.
Physically distorted political cartoons accompanied by whispered jeers remind us that fake news is not an exclusively
modern phenomenon. Here, the path to issuing the Emancipation Proclamation is literally surrounded by angry dissidents
arguing why it goes too far&mdash;or not far enough. It shatters the typical sterilized textbook portrayal of historical
documents, and confronts us with the reality: there is no obvious right or wrong. People will vilify you no matter what
decision you make. How will you choose?

The presentation of the emotional climax here is masterful. After the grinding stress of the Civil War, finally rays of
jubilation begin to break through&mdash;Lincoln is re-elected, Union victory seems assured, the Thirteenth Amendment is
passed! We've learned of the many tragedies that both the nation and Lincoln himself have suffered to get here, but it
all finally seems to have been worth it&#8230; and then you spy the entrance to Ford's Theatre at the end of the hall.
By the time we arrive at Lincoln's coffin, we can mourn the man personally, so acquainted are we with his sacrifices.

On the subject of the Emancipation Proclamation, Attorney General Edward Bate's views deserve special recognition, which
I would describe as "seventeen wrongs (almost) make a right." Quoting from the museum exhibit:

> Although against black equality, Bates gave his unreserved support to the Proclamation. He hoped and assumed that once
> free, all Negroes would leave the United States to colonize Central America.

While some historians disagree, this is widely considered to be the first bruh moment.

### State history museum

<aside><p>

I am a great lover of [Bernadette Banner](https://youtube.com/channel/UCSHtaUm-FjUps090S7crO4Q) and [Karolina
Zebrowska's](https://youtube.com/c/Karolina%C5%BBebrowskax) YouTube channels, where they discuss historical dress.
Seeing so many of the things they've discussed in real life as opposed to on my phone screen was remarkable. I loved
seeing [all the layers to an outfit](https://youtu.be/UpbiPXoSQ3E), a "[pocket](https://youtu.be/uaRoWPEUTI4)," a [foot
pedal sewing machine](https://youtu.be/ZppEd7zZxQI), and the [changing silhouettes of the
1800s](https://youtu.be/qkEGN-Nm5mU) for myself. I'm grateful this detour was indulged.

</p></aside>

With some extra time on our hands, we stopped by the [Illinois State Museum](http://www.illinoisstatemuseum.org/). (We
would have liked to tour the Capitol building proper, but it didn't seem accessible to the public at the time.) In
particular, the _[Fashioning Illinois:
1820&ndash;1900](http://web.archive.org/web/20210123005903/http://www.illinoisstatemuseum.org/content/fashioning-illinois-1820-1900)_
exhibit caught my wife's eye. The intricacies of the evolution of fashion were completely lost on me, of course, but the
exhibit was a stark reminder of how dramatically Western quality of life has increased since that time. A couple
examples in particular stood out. The first: an old folk song that jokingly describes how irascible wives are on "blue
Monday," the weekly washing day. This should perhaps not be altogether surprising, given that the process consisted of
tedious physical labour and harsh cleaning agents. The second item that caught my eye was a pair of hand-me-down
undergarments; after the original wearer passed away, her sister-in-law inherited them, and updated the name in the
waistband to match. At the risk of veering into economic debate, both examples make me better appreciate the
technological innovations that have eliminated those toils from my life (though of course, there is a separate
discussion to be had as to whether that toil has truly been eliminated, or merely transferred across the world,
conveniently beyond our notice).

<figure>
  <img src="capitol_building.jpg"
       alt="Outside the Illinois State Capitol">
  <figcaption>A striking view of the Illinois State Capitol</figcaption>
</figure>


### Lincoln's tomb

The next stop on our Lincoln tour was the family's final resting place, [The Lincoln
Tomb](https://www2.illinois.gov/dnrhistoric/Experience/Sites/Central/Pages/Lincoln-Tomb.aspx), located a few kilometres
north of the downtown area. Once again, my kudos to the Illinois Historic Preservation Divison: it was seamless to join
the next tour after we arrived, and the guides were friendly and knowledgable. The exterior features an obelisk (funny
how persistent that Egyptian influence seems to be), the requisite statue of the man himself, and four other statues
representing the branches of military service at the time (infantry, artillery, cavalry, and navy); those are built
partially from melted down canons from the war. The interior is a polished marble passageway that commemorates the
different periods of Lincoln's life with statues and snippets of speeches, with the family's monument halfway through.

![The exterior of the Lincoln family tomb](lincoln_tomb.jpg)

I wish I could say our time by the tombstone was a moment of silent, somber reflection. Instead, we felt vaguely rushed
and annoyed as the panting pack of Galaxy-S20-wielding boomers behind us inched ever closer. Yes, I'm being
uncharitable, but having to run mental COVID calculus yet again is a surefire way to ruin any moment.

Since I have no profound insights to offer from President Lincoln's final resting place, allow me to instead share the
story of a Mr. Roy Bertelli. Oak Ridge Cemetery is what you might expect&mdash;rolling green hills, birds singing
quietly, and rows upon rows of stately, marble tombstones proclaiming the dead. Like many, you may have come here to pay
your respects to Lincoln, but before you spot the obelisk adorning his tomb, you'll be accosted by an even more powerful
presence: MR. ACCORDION. MR. ACCORDION offers only questions, no answers&mdash;who was Roy Bertelli? Why did he like the
accordion so much? How in the world did he get his massive monument erected on what is ostensibly the footpath to
Lincoln's tomb? We found a full account of the late MR. ACCORDION on
[roadsideamerica.com](https://web.archive.org/web/20210507221417/https://www.roadsideamerica.com/story/19409). I won't
ruin the ending, but it's a hilarious "stick-it-to-the-man" story.

![Gravestone for Roy Bertelli, aka "Mr. Accordion"](mr_accordion.jpg)

### Final tour of downtown

<aside><p>
As I aimlessly looked at the endless haphazard stacks of books, I caught sight of a massive grey cloth bound book with
faint gold lettering on the spine way up on a top shelf: The Bayeux Tapestry. My stomach flipped. This book is an old
friend of mine. I did a project on the Tapestry that involved recreating a small section of the embroidery of the border
and it was this tome that I relied on as I studied the stitches. Thinking there was no way it'd be worth lugging this
thing back with us, as soon as I saw the price of $25, I knew it was already mine. It had been sitting on that shelf,
waiting for me to find it.
</p></aside>

Once again in the downtown core, we found ourselves with time for a few final stops before grabbing dinner and calling
it a day. We stumbled upon [Prairie Archives](http://prairiearchives.com/), one of those lovely bookstores where every
available surface is stacked with books. We had an indirect connection to the pastor of [First Presbyterian
Church](https://www.lincolnschurch.org/) and just happened to catch her by chance as we walked by. Reverend Phillips
graciously let us explore the church itself, which features absolutely stunning stained glass windows (including one of
the few existing signed Tiffany pieces), as well as the Lincoln family's former pew. It was truly an unexpected delight
to be able to take in. Thanks, Susan!

![Louis C. Tiffany's signature on a stained glass window](tiffany_signature_zoomed.jpg)

Whew&mdash;after a long day of exploring, there was one last piece of unique Springfield culture to experience: the
famous [horseshoe sandwich](https://en.wikipedia.org/wiki/Horseshoe_sandwich). A horseshoe sandwich is a slice of Texas
toast topped with your choice of meat (a veggie burger in my case), drowned in cheese sauce, and then buried in French
fries. We stopped by [Obed & Isaac's](https://www.connshg.com/obed-and-isaacs/springfield/) for ours. Heed my warning,
dear reader: apparently ordering a regular horseshoe means you get two of these concoctions, which is several meals'
worth of food. You'll probably want to opt for a "ponyshoe" instead, which is just a smaller serving. My overall
verdict? Pretty good! It's exactly what you'd expect, but if you're just looking for a different arrangement of
"cheeseburger + fries," this is a fun one. Also noteworthy: Obed & Isaac's had some tasty brews. Chief among them was
the watermelon sour, which is a genius flavouring for an underappreciated style. (Yes, it's basically just a watermelon
Jolly Rancher in beer form.)

![A horseshoe sandwich](horseshoe.jpg)

## Conclusion

All in all, we had a wonderful time in Springfield. While we won't be returning perennially, it really did feel like a
great place to take in some very interesting Midwestern culture. The walkability of the downtown core and its proximity
to the train station make it easy to recommend as a quick Chicago getaway for the historically inclined.
