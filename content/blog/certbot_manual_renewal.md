+++
title = "Manually renewing a TLS certificate with Certbot"
date = 2021-01-02
+++

One minor task I had to attend to over the holidays was renewing my Let's Encrypt certificate for the first time. Of
course, having originally set it up a couple of months ago, I didn't remember what to do. I'm writing up some notes for
myself to make this quicker the next time I need to do it, in ~60 days.

Note: you shouldn't have to do this manually (like I am)! `certbot`, a program to facilitate Let's Encrypt certificate
management, has many options for automation (and discourages you from doing things manually). Some silly quirks in my
current setup prevent me from leveraging this, but if you're setting up something yourself, I recommend you start there.

## Background

At time of writing, I have two separate entities under the simpsonian.ca domain:

1. This blog, which is actually hosted by [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/) (convenient and
   doesn't cost a dime for my usage). They also automatically provide and manage Let's Encrypt certificates for you, so
   there's nothing else to be done here&mdash;great!

2. A `local` subdomain, which points to a self-hosted [RockPro64](https://www.pine64.org/rockpro64/) (a powerful
   single-board computer from the fine folks at PINE64). Currently the only thing I run here is
   [Miniflux](https://miniflux.app/), an excellent RSS reader, which I serve behind Apache. For uninteresting reasons
   (stemming primarily from my own networking ineptitude), this isn't accessible from the public internet (hence
   preventing the automatic renewal options for `certbot`, I think). Configuring HTTPS for a local RSS reader probably
   isn't _strictly_ necessary, but who doesn't like looking at that nice green lock? Onward!

## Steps

### Renew certificate

First, we need to run `certbot` to kick off the renewal process. It will then walk us through the required steps. To
start, run:

```
sudo certbot certificates # to view existing certificates
# of course, remove --dry-run when ready
sudo certbot certonly --cert-name simpsonian.ca --manual --preferred-challenges dns --dry-run
```

I am using the DNS challenge because it doesn't require the host to be accessible from the public internet, and it
supports wildcard subdomains.

Note that `certbot` also takes care of some housekeeping for you; the symbolic links under `/etc/letsencrypt/live` will
point to the updated files, and previous versions will be kept around under `/etc/letsencrypt/archive`.

### Respond to the ACME challenge

Since we requested the DNS challenge above, we'll be asked to add certain DNS TXT records to prove we control the
domain. You should be able to control your DNS settings through your registrar's website. (I'm using Namecheap, which
annoyingly doesn't offer automatic Let's Encrypt certificates, unlike some other providers.) For a challenge like the
following, add an entry with type `TXT`, host `_acme-challenge`, and value
`ZyGx2BVpyK-Klkjmaikq7BJvuzgTp-c71-pMvRW6wDA`.

```
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Please deploy a DNS TXT record under the name
_acme-challenge.simpsonian.ca with the following value:

ZyGx2BVpyK-Klkjmaikq7BJvuzgTp-c71-pMvRW6wDA

Before continuing, verify the record is deployed.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
```

To confirm the new entries are present, you can use a tool like `dig`. Since DNS propagation takes some time, you might
need to wait a bit. I also recommend running the command below a few times to make sure all DNS servers have the updated
information.

```
dig -t TXT _acme-challenge.simpsonian.ca
```

### Restart Apache

There's a probably less disruptive way to do this, but I needed a quick restart for the new certificate to start being
served:

```
sudo systemctl restart apache2
```

### Remove unnecessary DNS entries

Finally, we no longer need the TXT entries we set up in step 2, so feel free to remove those.


And there you have it&mdash;you should now have some minty fresh TLS certificates.
