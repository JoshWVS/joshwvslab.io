+++
title = "Cal Newport's pragmatic pessimism"
date = 2023-05-15
+++

_(Note: this essay was originally submitted for the [Astral Codex Ten](https://astralcodexten.substack.com/) Book Review Contest 2023.)_

Reading Cal Newport's <cite>So Good They Can't Ignore You</cite> was a confounding experience. I generally agree with
his central premise that "follow your passion" is bad career advice, and that "skills trump passion in the quest for
work you love," but there's also a lot I disagree with along the way. More importantly though, Newport's dissection of
how to achieve conventional success is predicated on the continuation of the status quo. Fundamentally, this is a book
about how to make the best deal you can under the system that we've got. That may be a pragmatic point of view, but it's
one I find somewhat disheartening.

<!-- more -->

I begin this essay with a few specific points of disagreement with Newport's factual claims, then dive into some more
philosophical differences.

## Nits to pick

Newport's main mission is to convince us _not_ to follow our passion when choosing a career, but instead to develop our
"career capital" (aka "rare and valuable skills",[^networking] p. 48[^page-numbering]) in an area to the point where
we'll be able to command a job we love. Does he succeed in this argument? Mostly, I'd say, but I have a few points of
contention.

### 1. Debunking the "passion hypothesis"

Newport's first task is to refute what he calls the "passion hypothesis," that trite conventional wisdom that career
satisfaction can only be found by figuring out exactly what it is you are truly passionate about, and then finding a job
that lets you completely immerse yourself in it. Instead, he advocates amassing career capital by continually sharpening
your skills, then using that career capital as leverage to build your dream job (either in your current position, or by
switching to a new one). He dedicates a few chapters to this; there's one point in particular that bears greater
scrutiny.

As part of his literature review,[^lit-review] Newport looks at a paper from Amy Wrzesniewski.[^wrzesniewski]
Wrzesniewski's paper proposes that "most people see their work as either a Job (focus on financial rewards and necessity
rather than pleasure or fulfillment; not a major positive part of life), a Career (focus on advancement), or a Calling
(focus on enjoyment of fulfilling, socially useful work)."  The part Newport is most excited about is a survey of
college administrators, who were split roughly evenly between those three categories when surveyed. Newport's prized
result here, in his own words, is that "the strongest predictor of an assistant seeing her[^gender] work as a calling
was the number of years spent on the job." (p. 16&ndash;17) Aha! Work hard, put in the time to get good, and you'll end
up satisfied&mdash;that's Science&trade; now, baby! Well&hellip; not so fast. Newport fails to mention that this
involved a grand total of 24 respondents; in Wrzesniewski's own words "the results for our small sample of
administrative assistants cannot have more than heuristic value." More importantly, I think Newport is overlooking a far
more plausible explanation: if you hate your work, eventually you'll quit!

Specifically, my money is on a type of survivorship bias: remember, this survey is only capturing a snapshot of people
employed as administrative assistants _at the time of the survey_. Newport claims that since the study observed that the
more tenured assistants were more likely to label their work as a calling (in Wrzesniewski's parlance), his model is
supported by the data. But think back to that good ol' passion hypothesis again. In this model, on the day that somebody
starts a new position, they have a pretty good idea of whether it's a job, career, or calling for them, and that feeling
never changes (it simply arises from their passion, or lack thereof, for the position). Assuming the passion hypothesis
is true, what would we expect Wrzesniewski's survey results to look like? Well, among newer hires, there's probably a
mix of job, career, and calling types (some are true believers, others just needed any job they could get). But if this
is just a job to you, you're more likely to quit, since you're less personally invested in the position. So as job
tenure increases, we might also expect respondents to be more likely to be in the calling camp (again, because no
dispassionate people end up with such a long tenure; they've already quit before then). &hellip;hey wait a minute,
that's what the survey data showed!

In short, I don't think the design of the Wrzesniewski survey allows one to distinguish between Newport's model and the
alternative he is trying to discredit. (And that's assuming you surveyed enough people for it to be meaningful, which,
again, the primary author notes is not the case!) I'd love to see a study that surveys a group of people who just
started the same job, then follows up with the _exact same_ people years later&mdash;that would give us a much better
way to distinguish between our two competing hypotheses.

### 2. Refuting "the argument from pre-existing passion"

In his second of four "rules," Newport distinguishes between "_the craftsman mindset_, which focuses on what you can
offer the world," and "_the passion mindset_, which instead focuses on what the world can offer you" (p. 39, emphasis
original). Newport advises that you throw off the yoke of the passion mindset, adopt the craftsman mindset (which
involves extended, deliberate practice), build up your career capital and become "so good they can't ignore you." But to
do this, Newport needs to rebut a counterargument he's heard many times. These objectors propose that precisely because
the craftsman lifestyle can be so grueling, those that willingly subject themselves to it (for their chosen domain) must
already harbour some innate passion for the pursuit. This would upend Newport's vision; rather than craftsmanship simply
being a switch one needs to decide to flick, we're right back to "find your passion then pursue it
relentlessly"&mdash;the exact passion hypothesis Newport is arguing against. He dubs this argument "the argument from
pre-existing passion," and sets about refuting it two ways.

First, Newport argues "&hellip;let's dispense with the notion that performers like [accomplished guitarist] Jordan
Tice[^tice-and-martin] or Steve Martin are perfectly secure in their knowledge that they've found their true calling,"
because "&hellip;one of the first things you notice is their insecurity concerning their livelihood." (p. 40) In
particular, "Steve Martin was so unsure during his decade-long dedication to improving his routine that he regularly
suffered crippling anxiety attacks." (p. 41) Newport claims Martin persisted because he knew that by continually honing
his material, he would one day achieve success.

Let's recap Newport's view: a young Steve Martin dispassionately surveyed the field of possible careers, selected comedy
arbitrarily, then endured ten years of rocky mental health, secure in the knowledge that a good job was waiting for him
at the end of it. If passion isn't a factor here, it seems Martin could've saved himself some hardship by majoring in,
say, accounting&mdash;frankly, it's hard to believe Newport could ever recommend going into entertainment when the
risk-reward ratio is so much more favourable in other industries.

Come on! Of _course_ Steve Martin wasn't going to become an accountant, he was born to be an entertainer! It's in his
blood! Sure, maybe his commitment to the "craftsman mindset" led him to succeed where others failed, but what Newport
needed to show here is not "the craftsman mindset will lead to success," it's "passion is not a prerequisite for the
craftsman mindset." I don't think picking Steve Martin as his example helps his case!

Newport's second counter-argument is "more fundamental" (his words), namely: "I don't really care why performers adopt
the craftsman mindset." (p. 41) Uh, I think you _should_ care! Many of your claims depend on your ability to disarm this
argument! Now rather than trying to actually defend his position, Newport attempts this judo flip:

1. It's unclear why some top performers choose to adopt the craftsman mindset.
1. Regardless, adopting the craftsman mindset has instrumental value&mdash;it will help you get your dream job&mdash;so
   you should do it anyway.

But this simply assumes the conclusion&mdash;in Newport's own words on the previous page, the crux of the argument from
pre-existing passion is that "the craftsman mindset is only viable for those who already feel passionate about their
work." The whole point of the argument from pre-existing passion is that it might not be possible for everyone to simply
adopt the craftsman mindset for any given domain. That's what Newport needed to disprove here, but he again fails to do
so: maybe it really is the case that Steve Martin levels of practice are only possible if you feel Steve Martin levels
of passion!

Where does this leave us? I don't think this is fatal to Newport's position, but I end up at a somewhat different
conclusion than him. Rather than completely dispensing with passion, I think of it as the fuel to start the fire: find
something that a) you like (not necessarily _love_!), and b) has reasonable career opportunities. Then, work your little
tuchus off (following the craftsman mindset) to get good at it, by which point you'll be able to command a job you truly
enjoy. But what if there's nothing you like _that_ much? What if there's no subject that could ever interest you more
than the siren song of the TikTok algorithm? Unfortunately in that case, I don't have a good answer for you&mdash;and
neither, I suspect, does Newport.

### 3. Repeatability

One of the career profiles Newport offers in support of his thesis is that of Alex Berger, a successful TV writer. In
Newport's telling, Berger recognized that his career would live or die by the quality of his script writing, and
therefore practiced his craft relentlessly&mdash;writing day and night, and seeking every possible opportunity to get
feedback. In the end, it paid off, and he loves what he does; good for him! What bothers me though, is Newport's summary
(p. 68):

> What this story lacks in pizazz, it makes up in repeatability: There's nothing mysterious about how Alex Berger broke
> into Hollywood&mdash;he simply understood the value, and difficulty, of becoming good.

There's a very strong empirical claim implicitly being made here (hard work &rarr; success), and scant evidence on
offer. Give me a thousand would-be Alex Bergers, track every minute of their time, then check if hard work was the only
predictor of their success&mdash;personally, I have my doubts. It's undeniable that personal characteristics such as
race and gender have limited the careers of entertainers in the past, not to mention the blind, uncaring pronouncements
of chance.

Fundamentally, I just don't buy this story. I'm not doubting that Alex Berger worked hard, it's just that Newport's
 telling smacks of the lottery winner's TED Talk.

{{ image(
    src="./survivorship_bias.png",
    alt="xkcd comic wherein a lottery winner gives some dubious life advice",
    caption='"[Survivorship Bias](https://xkcd.com/1827/)," by [Randall Munroe](https://xkcd.com/about/), is licensed under [CC BY-NC 2.5](https://creativecommons.org/licenses/by-nc/2.5/)'
) }}

## Philosophical disagreements

With those object-level claims out of the way, allow me to wax poetic about some of my more fundamental differences with
Newport's views.

### Doing good better, according to Newport

There are cases where Newport thinks you should _not_ apply the craftsman mindset and instead walk away, such as when
"the job focuses on something you think is useless or perhaps even actively bad for the world." It was that criterion
that allowed him to "confidently delete these [high-paying Wall Street] offers as they arrived." (p. 57) Fair enough,
but here's a project that really excites Newport&mdash;courtesy of "cleantech venture capitalist" Mike Jackson (p. 70):

> The idea was simple: You give money to Mike, he does complicated transactions that only he and a few other energy
> regulation wonks really understand, and then he offers you certification that you've purchased enough carbon offsets
> for your business to be deemed carbon neutral.

Look, I want to assume the best of Mike. I truly, feverishly hope that his keen insight lead to increased sequestration
of carbon, slowing the warming of our planet&mdash;but when the mechanism is VCs organizing "complicated transactions
that only he and a few other energy regulation wonks really understand" leading to "certification," I start to get a
little nervous. I'm not under the illusion that being a bond trader is a noble order, but I suspect it generally
involves less temptation for greenwashing.[^green-bonds]

### How do we value people's skills and labour?

On this point, Newport does not equivocate: when trying to formulate a rule for when one should opt to take more control
of their day-to-day duties, Newport interviews [Derek Sivers](https://sive.rs/), who offers advice such as "Do what
people are willing to pay for", and "money is a neutral indicator of value." (p. 137) That's right kids, pushing opioids
and flavoured Juul pods are certified a-okay by The Market; but maybe give up on that non-profit of yours that doesn't
seem to be going anywhere? To be fair to Newport, this is immediately followed by "This isn't about making money", but I
find it hard to reconcile the two. As always, there's certainly some truth to this&mdash;if your hobby doesn't pay your
bills, it's not likely to be a good career!&mdash;but I think Newport takes it too far.

_(Also: Newport claims that "[Sivers] is more or less indifferent to money, having given away to charity the millions he
made from selling his first company" and began "living off the smallest possible amount of interest allowed by law."
Those are both basically _true_ statements, but with further context I find them misleading. In the interest of space,
I'll relegate that discussion to a spicy endnote.[^sivers-charity])_

I'm also not sure how to apply this reasoning to highly creative fields. How many of history's great artists went
unappreciated in their own time? Would our human history really be richer had they forwent their art in favour of a more
profitable skill? This is the bare, brutal logic of markets. I don't mean to push back against that logic too
strongly&mdash;we all need to eat, and markets can be an invaluable tool for coordinating all of the messy activity
required to make that happen. I only wish that Newport took the time to recognize that both underwater welding and
underwater basking weaving have their place in the tapestry of human activity, even if the latter commands a lower
market price.

### What the heck does Cal Newport know, anyway?

After Newport's taken you through his four rules for achieving career nirvana, you're directed to exit via the
innocuously-named Conclusion. You'd be forgiven for assuming that this might simply summarize those rules and throw in
an extra nugget or two of wisdom, but that is not the case. Instead, Newport drops the reverse-uno card he's had up his
sleeve the whole time: he's just beginning his own career!

Newport explains that he started dreaming up this book as he was finishing up as a postdoc and applying for academic
jobs; he mentions this in the intro (which I had completely forgotten about). Spoiler alert: happily, he was successful,
and "signed the deal for this book only two weeks after accepting [his] Georgetown offer." (p. 201) Good for him! He
uses the conclusion to demonstrate how he applied his four rules to his own job search.

That's all well and fine, but for a book full of career advice, I was hoping it would be more&hellip; grounded in
_personal_ experience? Newport's academic career trajectory is impressive, but it is also short and quite different from
the public- or private-sector paths most readers are following. I suppose the many "case studies" in the book of other
people's careers are intended to fill in this gap.

Of course, none of this necessarily means that Newport is _wrong_, but it feels funny to me for someone so early in
their career to proffer themself as a guru. Kinda like that time when I took ice skate advice from the bespotted teen at
the sports store and only noticed I bought myself women's skates when I got home.[^skates]

### If you can't make your own career capital, store-bought is fine

There's another part of the conclusion that rubs me the wrong way. Newport describes how he and a friend started up a
web design business in high school which earned them a nice chunk of change and some press coverage; he credits this
with playing a large role in his admission to an elite university. How exactly did the business work? Page 204 explains:
"Most of these contracts paid between $5,000 and $10,000, a healthy chunk of which we passed on to a team of Indian
sub-contractors, _who did most of the actual programming work_" (emphasis mine).

Gah!

There's nothing _wrong_ with this, per se. It certainly shows some entrepreneurial savvy on his part, and if those
Indian sub-contractors earned more than they otherwise might have on the local market, that's great&mdash;a win-win,
even! Except&hellip; doesn't it feel a bit strange that Mr. Career Capital got his start by, well, not
_misappropriating_, but perhaps _enlisting_ someone _else's_ career capital? Newport's message is work hard, follow the
craftsman mindset, and you're destined for success&mdash;but what if you happen to be born in South Asia? Should you be
satisfied being the engine of value for some American teen's geo-arbitrage biz? How have demographic, geographic, and
political factors beyond your control limited your ability to offer your career capital on the labour market? On these
questions, Newport remains demure.

### Purple Cows and bullshit

I know, I know&mdash;two criticisms of the conclusion and I'm still going; don't worry, we're nearly there.

To explain the importance of marketing your personal mission, Newport uses Seth Godin's analogy of "brown cows" and
"purple cows." Brown cows are so utterly ordinary that you look right past them without thinking; conversely, if you saw
a purple cow, you'd surely take note. Furthermore, purple cows don't just happen by accident; they're the result of
deliberate marketing. Newport is practicing what he preaches here: "the book you're holding was conceived from the very
early stages with the hope of being seen as 'remarkable.'[^remarkable]" (p. 191)

Newport correctly identifies that you need some measure of attention to "succeed," at least in that legible,
achievement-oriented way. Just being _correct_, _true_, or _good_ is not enough; you can have the world's smartest cow
and no one will notice unless it's purple. But this is a slippery slope: if you get really good at dyeing
cows&mdash;something for which you'll be rewarded!&mdash;you might end up being "successful" without actually being
_good_ or _right_. (See: pretty much every self-help book ever.)

The single best example I can give comes directly from Newport's website.[^email-example]

{{ image(
    src="./the_duality_of_email.jpg",
    alt="A screenshot of Newport's website, showing an ad for his book <cite>A World Without Email</cite>, next to a banner inviting you to sign up for blog updates via email",
    caption="The Duality of Email. (My RSS reader doesn't find a feed for the blog.)"
) }}

I haven't read <cite>A World Without Email</cite>, and I'm sure Newport has a way to square this circle. (Maybe rather
than completely abolishing email, you check it once a day at 5pm to avoid interfering with your "deep work.") But
still. How can this come across as anything other than disingenuous?

### Fine, let's do the motorcycle book

In many ways it seems to me that <cite>[Zen and the Art of Motorcycle
Maintenance](https://en.wikipedia.org/wiki/Zen_and_the_Art_of_Motorcycle_Maintenance)</cite> (by Robert Pirsig) is the
antithesis of <cite>So Good They Can't Ignore You</cite>.

For the uninitiated: I claim no deep understanding of the nuances of the former, but it is undeniably an extended
meditation on the definition and meaning of "Quality." For the gory details, consult the original, but for our purposes
I only want to draw out one concept it highlights: [_arete_](https://en.wikipedia.org/wiki/Arete).[^zmm-arete] _Arete_
is an ancient Greek concept that can be roughly translated as "excellence" or "virtue;" living up to your full potential
and making the most of all your abilities is a perfect display of _arete_. More specifically though, _arete_ isn't
performative, or done for the sake of praise, it is done for one's self&mdash;it looks not outward, but inward.

In <cite>So Good They Can't Ignore You</cite>, I don't see Newport as valuing _arete_. To him, quality is a _means_ to
an end, not the goal itself&mdash;quality is just the seed you water with external attention to cultivate a legible
success; the success is what he's really after. That's not a knock against him! He seems to know what he wants, and he
appears to be quite effective at getting it.

But personally, I feel differently. I don't _dislike_ success of course; nor dare I deign denigrate it, but simply put:
if you force me to choose between the up-and-coming VC luminary and the modest Linux kernel hacker toiling away simply
for the love of the craft, my sympathies are with the latter.

## My takeaways

So, where does that leave us?

Overall, I'm glad to have read <cite>So Good They Can't Ignore You</cite>. I think it does have some good (and
actionable) advice for me; it might for you as well. Autonomy is crucial for liking your job&mdash;if you don't have it
now, figure out a way to get it!

That being said, I trust Newport a little less after having read this. There's some genuine pearls of wisdom here, and
likely in his other stuff as well, but his prioritization of success over truth makes me worry I might have to sift
through manure to find them.

At the end of the day, I don't think Newport's big idea is _wrong:_ pick something that marginally interests you with
some career opportunities, get good at it, sprinkle a little puffery on top, and&mdash;assuming there are no structural
constraints limiting your opportunities&mdash;you can build a satisfying career for yourself. So sure, maybe skills
really do trump passion.

What I don't understand is how Newport is perfectly willing to just leave it at that. He'll teach you how to play the
game like a pro, but he never stops to contemplate how the rules came to be, or if we should try to change them.
Personally, I find that myopia a little depressing, but I won't dispute its effectiveness.

<hr>

[^networking]: Interestingly, Newport never mentions one's personal connections/network as being important in finding a
    job you love. Frankly, I find this dumbfounding, since I think of this as being some of the most commonly-offered
career advice. I think Newport overlooks it because he's very focused on legible measures of personal success (e.g.,
getting great test scores; we'll get into that later).

[^page-numbering]: The page numbers I'm providing are for the edition with ISBN 978-1-4555-0912-6.

[^lit-review]: It's not exactly extensive; by my count there are three references to academic literature in this section
    of the book (and as we'll see, Newport's summary of that research can be&hellip; selective). This is more interviews 'n' anecdotes territory. Of course, that doesn't stop the publisher from describing this as "an _evidence-based_ blueprint for creating work you love" (reverse flap; emphasis mine).

[^wrzesniewski]: Wrzesniewski, Amy, Clark McCauley, Paul Rozin, and Barry Schwartz. 1997. "Jobs, Careers, and Callings:
    People’s Relations to Their Work." <cite>Journal of Research in Personality</cite> 31 (1): 21–33. Or at least,
    that's what the library website spits out for me.
	
[^gender]: Newport's language; the paper seems to stick to "administrative assistants"/"respondents."

[^tice-and-martin]: Newport uses Jordan Tice and Steve Martin as examples of people employing the craftsman mindset; the
    book's title is taken from a quip by Martin. Interestingly, [Tice's
    website](https://web.archive.org/web/20220524150350/https://www.jordantice.net/about) mentions he's worked with
    Steve Martin&mdash;either I missed where Newport mentioned that, it's a weird coincidence, or Newport happened upon
    the connection as well?

[^green-bonds]: Although in the era of green bonds, perhaps not?

[^sivers-charity]: First of all, kudos to Sivers for seemingly being totally transparent with this whole arrangement. In
    a [blog post](https://web.archive.org/web/20221224220146/https://sive.rs/trust) from 2009, he clearly explains that
he transferred the company he founded to a self-founded charitable trust for music education; when his company was later
sold for 22 million bucks, the proceeds went to the trust. Furthermore, while he is alive, that trust "pays out 5% of
its value per year to [him]"&mdash;so, a cool $1.1 million per year to start? (In the comments on that post, Sivers
notes that "5% is the minimum payout required by law" for such a trust&mdash;okay, but it didn't _have_ to be this kind
of trust, right?) To be clear, I emphatically do _not_ mean to disparage this kind of giving&mdash;wanting to donate a
lump sum while preserving some kind of income stream is a-okay in my books&mdash;but uh, at a 5% annual drawdown, is
this really sustainable? Obviously you can invest the trust's assets (Sivers notes this as well), but I'd be very
interested in seeing the trust's financial statements from 2009 up to now.  So, yeah; "living off the smallest possible
amount of interest allowed by law"&mdash;technically true, but a seven-figure stipend seems pretty comfortable
nonetheless.

[^skates]: It's fine! It's fine. I'm a mature, enlightened man, and wearing women's skates, emblazoned with "ALEXIS ICE"
    and the breast cancer awareness ribbon in no way threatens my sense of masculinity. I just&hellip; might have picked
something else if I had realized. [I am not a clever
man.](https://web.archive.org/web/20221228235332/https://www.buttersafe.com/2008/10/23/the-detour/)

[^remarkable]: Don't sweat the quotation marks on remarkable; Newport's just using them to mean "the literal sense of
    compelling people to remark about it" (p. 190), rather than a second sense, which he subsequently discusses.

[^email-example]: This screenshot was taken at the time of writing; as I edit this some time later it appears the
    website has undergone a redesign. I believe my point stands.

[^zmm-arete]: Quoting from <cite>Zen and the Art of Motorcycle Maintenance</cite> (p.
    I-don't-actually-own-this-book-and-just-found-a-sketchy-pdf-online):

> "What moves the Greek warrior to deeds of heroism," Kitto comments, "is not a sense of duty as we understand
> it&hellip; duty towards others: it is rather duty towards himself. He strives after that which we translate 'virtue'
> but is in Greek areté, 'excellence'&mdash;we shall have much to say about areté. It runs through Greek life." There,
> Phaedrus thinks, is a definition of Quality that had existed a thousand years before the dialecticians ever thought to
> put it to word-traps. Anyone who cannot understand this meaning without logical definiens and definendum and
> differentia is either lying or so out of touch with the common lot of humanity as to be unworthy of receiving any
> reply whatsoever. Phaedrus is fascinated too by the description of the motive of "duty toward self" which is an
> almost exact translation of the Sanskrit word dharma, sometimes described as the "one" of the Hindus. Can the dharma
> of the Hindus and the "virtue" of the ancient Greeks be identical? Then Phaedrus feels a tugging to read the passage
> again, and he does so and then&mdash;what's this?! -- "That which we translate 'virtue' but is in Greek
> 'excellence.'"
>
> Lightning hits!
>
> Quality! Virtue! Dharma! That is what the Sophists were teaching! Not ethical relativism. Not pristine "virtue." But
> areté. Excellence. Dharma! Before the Church of Reason. Before substance. Before form. Before mind and matter. Before
> dialectic itself. Quality had been absolute. Those first teachers of the Western world were teaching Quality, and the
> medium they had chosen was that of rhetoric. He has been doing it right all along.

[^typos]: For you, the particularly dedicated reader, let me prove that I actually read this thing by offering a couple
    of typos in this (unlinked) endnote.

1. On page 82, elite chess players are "pouring" over books, which tends not to be good for the books' longevity;
2. On page 222, we learn of Esther Duflo, "who won a MacArthur 'Genius Grant' for her work evaluating _ant-poverty_
   programs" (emphasis mine). Now, I fact-checked this one&mdash;her research focus seems to be _anti_-poverty programs,
but this being an EA-friendly space, it was worth confirming. If only FTX was still around&mdash;there _are_ a lot
of ants, after all&hellip;
