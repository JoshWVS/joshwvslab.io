+++
title = "Fifty weeks of jazz"
date = 2025-01-14
updated = 2025-03-02
+++

Right&mdash;the holidays are over, the Christmas tree is fuel for the fire, and the worst of the NYE champagne bottle
shards have been swept up; it's time to get serious. As in, resolutions.

<!-- more -->

Over the past few months I've been trying to listen to more jazz. I can't quite remember what started this whole
project, but now I won't rest until I've achieved my own transcendental [jazz
trance](https://www.youtube.com/watch?v=bKwQ_zeRwEs).

As it so happens, while checking out the local flea market recently I chanced upon some very relevant
[box](https://www.discogs.com/release/2375584-Various-The-Perfect-Jazz-Collection)
[sets](https://www.discogs.com/release/2966397-Various-The-Perfect-Jazz-Collection-2), comprising some fifty classic
jazz albums.

So here's the plan: at one album per week, that gives me about a year of autodidactic jazz education, plus a buffer at
the end for some self-reflection. I don't have a textbook or any other proper guide; I'm just going to listen to the
albums, binge some Wikipedia pages, and see where that gets me in a year.

Fair warning: this post is just going to be me brain-dumping my uneducated, undiscerning thoughts on these albums. I
don't imagine it will be entertaining or useful in any way for literally anybody else, but hey, you're the one reading
it.

{{ fifty_weeks_of_jazz_heading(n=1, title="Louis Armstrong Plays W. C. Handy", mb_uuid="3c6e5005-cad2-4444-825f-71e4b8324d51")}}

Not really knowing anything about these box sets in advance, I embarked on this project with some trepidation that I had signed myself up for fifty weeks of crap. By the end of this album's first track, I was a believer.

The top-notch musicianship and Satchmo's[^satchmo] trademark gravelly crooning are already reason enough to listen, but
I can't emphasize enough how much _fun_ this album is: the frequent banter between Louis and [Velma
Middleton](https://en.wikipedia.org/wiki/Velma_Middleton) is charming, and the whole thing is joyous and downright
raucous throughout. I mean, just listen to this laugh:

<audio controls src="louis_armstrong_laugh.mp3"></audio>

One thing I don't understand: [W. C. Handy](https://en.wikipedia.org/wiki/W._C._Handy) was the "Father of the Blues,"
and most of the songs here have "blues" in the title&mdash;obviously there's lots of shared heritage between jazz and
blues, but where exactly does that dividing line lie? And what is this exactly?

_Favourite track:_ St. Louis Blues

{{ fifty_weeks_of_jazz_heading(n=2, title="Sarah Vaughan in Hi‐Fi", mb_uuid="36e093d0-c78c-3030-9753-f0094daf8ab0")}}

This one is a slower burn for me: obviously Sarah Vaughan is an incredibly talented vocalist, but this doesn't quite get
my blood pumping like the last album; sometimes her melodies go a little too all over the place for my taste. That said,
I'd gladly put this on for a nice romantic dinner (skip "Mean to Me" in that case, though).

Also, apparently Miles Davis is playing the trumpet for most of these tracks? We'll get back to him soon enough&hellip;

_Favourite track:_ Nice Work If You Can Get It

{{ fifty_weeks_of_jazz_heading(n=3, title="The Jazz Messengers", mb_uuid="a77f219c-d342-4ae3-bdab-352751f19ba7")}}

After Ms. Vaughan's lovely but somewhat sedate tunes, I was hoping for this week to bring a little more energy, and The
Jazz Messengers definitely delivered on that front. Wikipedia tells me we're in [hard
bop](https://en.wikipedia.org/wiki/Hard_bop) territory now, and it's not hard to hear why it might've earned that
moniker.

Art Blakey is undeniably a monster on the drums, but some of his solos end up sounding a little&hellip; samey to me?
Take the first big solo from Infra-Rae: it starts to get repetitive for me in a way that I've never felt when listening
to say, [Neil Peart](https://en.wikipedia.org/wiki/Neil_Peart). Listen for yourself and see if you agree:

<audio controls src="infra-rae_solo.mp3"></audio>

Not too much more to say on this one, other than that I'm glad to have some high-tempo, vocal-free jazz&mdash;good music
for when you really need to get something done. Oh, and "Carol's Interlude" on this album really reminds me of
"Epistrohpy" from another [Monk/Coltrane
album](https://musicbrainz.org/release-group/e0b33a23-c61c-34a1-a903-47a55255ede5) I have.

_Favourite track:_ Hank's Symphony

{{ fifty_weeks_of_jazz_heading(n=4, title="Lady in Satin", mb_uuid="cf7bc239-a3ba-3d87-80e5-261f750cb560")}}

Given that the primary feature here is a solo female vocalist&mdash;Billie Holiday&mdash;I couldn't help but compare
this album to the [earlier Sarah Vaughan album](#week-2). I prefer this one: the lush orchestral backing really helps
create a sweeping, dramatic sense of romance, as opposed to Vaughan's lighter, airier mood.

Immediately, one can hear some fragility and rasp in Holiday's voice; she comes across as much more "mature" than
Vaughan to my ears. What I hadn't realized was the sad explanation: at the time of the recording, Holiday was in poor
health, after having weathered decades of damage from alcohol, hard drugs, and abusive relationships. She recorded this
album in February 1958 and passed away barely 18 months later. The critical consensus seems to be that the album packs a
huge emotional punch, but that Holiday had lost some of her range and force by this point&mdash;I'd like to return to
her earlier work and compare for myself.

I suspect this album will also hold a special place in my heart owing to the circumstances of my first listen: not
wanting to miss a week, I had copied this album to my phone in advance of a trip to the Netherlands; I listened to it
together with my wife on the train as we watched the Dutch countryside go by.

Two miscellaneous closing thoughts:

- It's a dang shame that she never had a chance to sing a Bond theme (and not only because that would up the
  [Billie&ndash;Bond count](https://en.wikipedia.org/wiki/No_Time_to_Die_(song)) to two).
- Thanks to Wikipedia, I learned that there's a [crater on Venus](https://planetarynames.wr.usgs.gov/Feature/2542) named
  after her.

_Favourite track:_ For Heaven's Sake

{{ fifty_weeks_of_jazz_heading(n=5, title="Kind of Blue", mb_uuid="8e8a594f-2175-38c7-a871-abb68ec363e7")}}

We're into the heavy hitters now&mdash;this is one of the few jazz albums I picked up prior to this experiment, based on
its universal praise. What is there for me to say that hasn't already been said? It's _Kind of Blue_&mdash;if any album
_is_ jazz, it's this one.

From what I've been able to gather online, _Kind of Blue_ is notable for pioneering modal jazz (in response to [hard
bop](#week-3)). My limited understanding is that in hard bop, melodies were largely driven by quick chord progressions:
soloists displayed their incredible virtuosity by continually adapting to the rapidly shifting tonal centre dictated by
the chord changes. In contrast, modal jazz uses very few, slow-moving chords&mdash;giving soloists more latitude to
develop their melodies without being constrained by the strictures of the chord progression. (As for what exactly
musical "[modes](https://en.wikipedia.org/wiki/Mode_(music))" are and what their significance is, I still don't have a
clear understanding.)

I'm sure I'll be listening to and learning from this one for many years to come, along with the rest of the world.

_Favourite track:_ So What

{{ fifty_weeks_of_jazz_heading(n=6, title="Time Out", mb_uuid="035a7881-3e2c-39d2-b110-fe26a4de94e5")}}

This album was an immediate revelation for me. I've listened to some great stuff already, but _Time Out_ had me hooked
from the first few bars&mdash;the opening rhythm of Blue Rondo à la Turk is now permanently tattooed on my brain.
Perhaps that's no coincidence; _Time Out_'s primary innovation is its usage of time signatures that were previously
uncommon in jazz (e.g., {{ time_signature(num=9, denom=8) }} in Blue Rondo à la Turk; {{ time_signature(num=5, denom=4)
}} in Take Five). If, like me, you're struggling to understand the nuances of {{ time_signature(num=9, denom=8) }}
rhythm, here's a helpful explanatory video [from
Wikipedia](https://en.wikipedia.org/wiki/File:Blue-Rondo-%C3%A0-La-Turk.theora.ogv):

<iframe src="https://commons.wikimedia.org/wiki/File:Blue-Rondo-%C3%A0-La-Turk.theora.ogv?embedplayer=yes" style="max-width: 100%" width="512" height="288" frameborder="0" loading="lazy" allow="autoplay; picture-in-picture" allowfullscreen></iframe>

Got it? Good.

Another thing that stood out to me was how&hellip; classical this album sounds? Even on my first listen, something here
struck me as familiar in a way I hadn't experienced with the earlier albums.

One more embed before we go: Canadian film animator [Steven Woloshen](https://en.wikipedia.org/wiki/Steven_Woloshen)
produced a short film set to Take Five that I think is pretty neat:

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/215511559?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture; clipboard-write; encrypted-media" style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Cameras Take Five"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

_Favourite track:_ Blue Rondo à la Turk

{{ fifty_weeks_of_jazz_heading(n=7, title="First Time! The Count Meets the Duke", mb_uuid="65890654-7523-33e0-a825-2bf9fe8efe41")}}

As [previously mentioned](#week-5), I had listened to a grand total of three jazz albums before setting out on this
expedition. We've already encountered _Kind of Blue_; one of the other two was _[Side by
Side](https://musicbrainz.org/release-group/f8d5a1fd-04c9-338c-b1a4-811e3b65f684)_&mdash;_Side by Side_ was mostly a
Johnny Hodges album, but they stamped his better-known bandleader Ellington's mug on the cover, presumably to juice the
sales.

I bring all of this up because this week's album features some of the same suspects: Ellington has gathered together his
big band (Hodges included), but they're not alone: Count Basie&mdash;another big band luminary&mdash;is on set with his
crew, and out to wage Big Band "Battle Royal" (_sic_)! The stereo mix is intended to capture the conflict: Basie's group
is heard on left channel, while Ellington's is on the right.[^basie-ellington-mnemonic] To my undiscerning ear, the
result sounds as cooperative as it does antagonistic, but that doesn't stop it from being a blast through and through.
At peak moments, the brass can get a little screechy; I don't think it's ever over the top but my wife is not on board
with it. (E.g., listen to the climax of Battle Royal&mdash;it's on the verge of cacophony, but I think it remains on the
"impassioned finale" side of the line.)

I've bandied about "big band" above with any proper explanation, so I suppose I ought to fill in some of those details:
big band is exactly what it sounds like&mdash;a musical ensemble for jazz that consists of at least ten (and often 17 or
more) musicians (usually divided into saxophones, trumpets, trombones, and rhythm). Compared to earlier jazz ensembles
(which typically comprised 4&ndash;5 musicians), big bands were obviously larger, but also had a greater focus on
written compositions (owing to the increased difficulty of coordinating solos across a much bigger group). Big bands
reached peak popularity in the late 1930s/early 1940s alongside "swing," the subgenre of jazz they usually played. Swing
was very popular at dance halls; as you might guess from the name, it features a "swinging" rhythm (usually by accenting
the second and fourth beats). Both Duke Ellington and Count Basie were both highly regarded big band leaders who shaped
the development of swing (Ellington in particular was also a prolific composer and arranger).

tl;dr:
<dl>
    <dt>Big band</dt>
    <dd>Jazz group of at least ten musicians (probably more).</dd>
    <dt>Swing</dt>
    <dd>Subgenre of jazz with a strong emphasis on a "swinging" rhythm; frequently played at dance halls.</dd>
</dl>

_Favourite track:_ Wild Man (aka Wild Man Moore)[^wild-man-notes]

{{ fifty_weeks_of_jazz_heading(n=8, title="Parole E Musica", mb_uuid="9ced0e26-5eab-4169-82d7-bec404815bcb")}}

Eight weeks in, and we've arrived at a dubious honour: this is the first album where the top-billed performer ([Helen
Merrill](https://en.wikipedia.org/wiki/Helen_Merrill) in this case) is still alive&mdash;as I write this in February
2025, she's a sprightly 95 years old according to Wikipedia. Perhaps more excitingly, we've also got our first duplicate
track: we first heard "You Don't Know What Love Is" in [week 4](#week-4) (I give the edge to Merrill).

This album has an interesting gimmick. For each "song," there's two tracks: the first is a spoken word version of the
song's lyrics _in Italian_; the second is the real song (with Merrill singing in English). Is it completely unnecessary
and self-indulgent? Yes. Do I love it, and does it make me pretend I'm living on the Amalfi Coast in
[Ripley](https://en.wikipedia.org/wiki/Ripley_(TV_series))? Also yes.

Also, this is the third album to feature a lovely leading lady, and I have a confession to make: while there's no
denying the sublimity of Merrill's singing, it's pretty clear to me by now that this isn't what I want out of my
jazz&mdash;I'd almost always sooner reach for any of the instrumental albums we've heard, or Armstrong's bombastic
vocals.

One final bit of trivia for the week: this might be the first you've heard of Helen Merrill, but you surely know one of
her son's songs: he wrote the original version of "[I Love Rock 'n' Roll](https://www.youtube.com/watch?v=Ak5xWekNfSo)."

_Favourite track:_ Why Don't You Do It Right

{{ fifty_weeks_of_jazz_heading(n=9, title="Tijuana Moods", mb_uuid="fab2e359-265d-35cc-897e-64b11b59d18c")}}

Charles Mingus was another one of those "big names" from the jazz world that I recognized, but had never actually
listened to until this week. _Tijuana Moods_ sounds quite different than anything else we've heard so far; I'd describe
it as more assertive and even a bit combative at times (though smooth elsewhere; e.g., on "Flamingo"). This certainly
isn't "easy listening;" compared to the other albums it feels less eager to please the listener and more
experimental&mdash;maybe it's fair to call it a concept album? As you might've guessed, the album was inspired by
Mingus' time in Tijuana; throughout the album, even my ignorant ears can clearly discern the Latin beats and motifs
getting the blood pumping as the jazz takes off.

But the track that's stuck with me the most isn't even from the original release of the album. "A Colloquial Dream
(Scenes in the City)" was only added on some later CD releases: it's nearly 11 minutes long and merges jazz with
spoken-word poetry; it features a down-on-his luck narrator recounting his love for jazz and his struggles living in
Harlem. At first, I didn't like it, and I wasn't convinced it was likeable by anyone&mdash;a fun novelty, sure, but is
this still even music? But after listening to the album all week long,
[propinquity](https://en.wikipedia.org/wiki/Propinquity) did its thing, and that narrator now lives rent-free in my head
(a good development for him given his situation with the landlady). This isn't the kind of song you can blast at the gym
or put on your party playlist, but if you're in a ruminative mood&mdash;possibly with a dram of whisky in
hand&mdash;there's a real depth of soul here.

Now catch this&hellip; DIG!

_Favourite track:_ A Colloquial Dream (Scenes in the City)

<hr>

[^satchmo]: One of Louis Armstrong's nicknames&mdash;look, I'm learning already!

[^basie-ellington-mnemonic]: Easy mnemonic to keep it straight: try making lowercase "b" or "d" letters with both your
    hands (i.e. make an "OK" gesture, then straighten your third, fourth, and pinkie fingers). You'll notice that on
your left you have Count <em>b</em>asie, while on your right you have <em>d</em>uke Ellington.

[^wild-man-notes]: I'm a sucker for some jazz flute (which also featured in a track on _Side by Side_). Wild Man brings
    that, and the ending gives me chills&mdash;the pianos trading off final high & low notes is perfect.
