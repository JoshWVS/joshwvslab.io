+++
title = "Tom Brady Super Bowl"
date = 2021-03-20
+++

O, how lucky are we! The ancient Greeks had to invent their Heracles&mdash;we have been gifted ours, and his name is Tom
Brady.

To hate a champion is the mark of a failure. Those driven to jealousy by his primacy eagerly await his downfall. But for
a God, ascendance is inevitable&mdash;draw closer, and hear the Oracle's vision.

Witness, child, the Tom Brady of next year: flashing an easy, boyish smile as he jogs to the field&mdash;pearlescent
teeth framed by lips plump with filial love&mdash;unfettered by the shackles of age; the confidence of one who has
devoted a lifetime to winning.

A decade from now: Tom Brady, the first bionically augmented man, returns to the spotlight, once again rising above the
objections of his detractors. They claim that he is "cheating;" that his updated body is an "unfair advantage." Those
are the pitiful cries of cowards who know full well they will fall to him in direct contest. To think the flesh of the
man is the source of his strength? An absurdity. Tom Brady's power stems not from his body, nor from his mind&mdash;it
comes from the radiance of his soul itself, for which his corporeal form is merely an indifferent chauffeur.

I know not how next century's Tom Brady shall present himself. But a millennium from now, child, look to the heavens,
for there you will see cosmic intelligence Tom Brady: moving the stars and the planets themselves, invisible
choreographer of this celestial ballet; adjusting their paths _just so_, gently guiding the pigskin caught in their
orbits to yet another galactic touchdown.

Our reality is the chance confluence of universal rhythms: every month, there is a new moon; every quarter, a new
season; and every year, there is a new Super Bowl. And every year, Tom Brady wins it.

Tom Brady Super Bowl. SUPER BOWL TOM BRADY!
