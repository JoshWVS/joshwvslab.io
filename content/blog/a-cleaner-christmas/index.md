+++
title = "A cleaner, greener Christmas"
date = 2024-01-01
+++

Here's a quick quiz for you: what holiday season tradition is the #1 contributor to the ongoing climate crisis?

<!-- more -->

That's right, it's Santa Claus. No, not because of the toy production facilities, nor the aerial delivery&mdash;those
went electric years ago&mdash;but rather due to one stubborn custom: the ol&rsquo; lump of coal.

Coal is, in technical terms, the absolute _worst_. It's responsible for [more CO<sub>2</sub> emissions than any other
fuel](https://ourworldindata.org/grapher/co2-emissions-by-fuel-line) on Earth, and now that Father Christmas has got
over [two billion tykes](https://ourworldindata.org/grapher/population-by-age-group) to visit&mdash;most of whom aren't
exactly angels&mdash;it's pretty clear that this bituminous punishment isn't sustainable.

The most obvious solution, of course, would be to raise all children with such love, wisdom, respect, and kindness that
they end up suffuse with fellow feeling, and behave benevolently in every scenario; with no one on the naughty list,
there would be no need to schlep around those sooty sacks. But I am no fool; that is plainly impossible. Instead, we
will have to content ourselves with the second-best option: 100 microgram pellets of enriched uranium.

The advantages are multifold. First and foremost, burning less coal means fewer toxic byproducts, such as greenhouse
gases, sulfurous compounds (which cause acid rain), and heavy metals. Secondly, the logistics would be simpler and more
efficient. All that coal takes up a lot of space (don't be duped by this notion of "Santa's magic sack"&mdash;there are
[firm physical limits](https://en.wikipedia.org/wiki/Schwarzschild_radius) in this universe, and no quantity of
"yuletide spirit" will allow one to circumvent them); the uranium pellets could be stored much more compactly, reducing
round trips to the North Pole for delivery. Finally, there are educational benefits&mdash;it doesn't take a child
prodigy to figure out how to get heat from coal, but extracting useful energy from fissile uranium (without going
supercritical) is an enriching DIY activity for the whole family.

But where is Santa going to get his hands on all of that yellowcake, you ask? Luckily for him, the world's largest
uranium mine, [McArthur River](https://en.wikipedia.org/wiki/McArthur_River_uranium_mine), is in northern
Saskatchewan[^anagram]&mdash;just a hop, skip, and a jump from Kris Kringle's home base (especially if he retrofits the
sleigh with a small nuclear reactor).

Only one question remains: why 100 micrograms? It's simple. I figure that a pretty generous lump of coal would be about
the size of my fist.[^humblebrag] A basic Archimedean exercise in the kitchen sink revealed that my fist displaces about
320g of water, so call it 320 cm<sup>3</sup>. The density of coal is 1.05 g/cm<sup>3</sup>,[^coal-mass-density] and the
_energy_ density of coal is 24 megajoules per kilogram.[^coal-energy-density] Multiply those out (don't forget to
convert grams to kilograms!), and you get roughly 8 megajoules of energy in a typical Christmas morning disappointment.
Divide that 8 MJ by the energy density of uranium (76,000,000 MJ/kg[^uranium-energy-density]) to get the final result of
~100 &micro;g&mdash;WolframAlpha tells me that would translate to a 110 &micro;m sphere of uranium.

I hope you'll support me in switching to uranium so that we might have a greener Christmas. And by that, I don't just
mean "more eco-friendly"&mdash;it does wonders in [glassware](https://en.wikipedia.org/wiki/Uranium_glass) too.

{{ image(
    src="./uranium_glass_glowing.jpg",
    alt="Uranium glass giving off a very Christmassy green glow",
    caption='"[Uranium glassware glowing under ultraviolet
light](https://commons.wikimedia.org/wiki/File:Vasline_glass_glowing.jpg)," by Wikipedia user Realfintogive, is licensed
under [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/). Apparently it&apos;s all safe to use.'
) }}

It won't be easy, but with a [CANDU](https://en.wikipedia.org/wiki/CANDU_reactor) attitude, we'll get it done. Happy New
Year.

<hr>

[^anagram]: What's more, "Saskatchewan" is an anagram of "Santa hawks CE." As we all know, CE stands for "clean
    electricity," so that's a pretty neat coincidence.

[^humblebrag]: Obviously _I_ never received coal from Santa as a child, so I'm guesstimating here.

[^coal-mass-density]: Per WolframAlpha.

[^coal-energy-density]: Per [this xkcd](https://xkcd.com/1162/).

[^uranium-energy-density]: Ibid.
