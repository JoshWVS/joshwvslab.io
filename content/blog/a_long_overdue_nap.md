+++
title = "A long overdue nap"
date = 2021-06-16
+++

I'm no sysadmin greybeard, but last year I set up a [ROCKPro64](https://www.pine64.org/rockpro64/), mostly to self-host
[Miniflux](https://miniflux.app/), a RSS reader. (For what it's worth, I've greatly enjoyed this setup and plan on
writing more about it later.)

One unexpected outcome of this is that I now totally _get_ the uptime fetish some people seem to have. It's the original
"streak" incentive! I've done precisely zero maintenance since setting up the server, and here's where things stand
today:

```
josh@treebeard:~$ uptime
 09:56:59 up 252 days, 11:50,  1 user,  load average: 0.00, 0.00, 0.00
josh@treebeard:~$ # goodnight, sweet prince
josh@treebeard:~$ sudo shutdown now
```

Unfortunately, an upcoming international move means this streak is about to come to an end. Sleep tight,
`treebeard`&mdash;there will be a whole new world waiting for you when you wake up!
