+++
title = "My ChatGPT Moment"
date = 2022-12-14
+++

Well, it seems the thing _du jour_ is to share one's ChatGPT experiences, so here's mine: yesterday marked the first
time in my life that directly asking a question to an AI provided a clearer, better answer than searching the internet
myself.

Recently, I've been hacking a bit on [a project](https://gitlab.com/JoshWVS/asteria) to drive the LEDs on my
fancy-shmancy new computer case (powered by the awesome [OpenRGB](https://openrgb.org/)). Specifically, I was adding a
"progress bar" effect, where the strip of LEDs are initially all turned off (to represent 0%), then illuminate one by
one as your metric of interest approaches 100%. After slapping together a prototype, I wanted to test it by using it to
display my PC's memory usage. To do so, I would need a simple command to use a ton of memory, so that I could run it and
see the strip of LEDs update accordingly.

Looking back on it now, perhaps the obvious solution would just be to fire up one's <abbr>REPL</abbr> of choice and
start manually allocating/deallocating large objects, but in the moment, I dismissed that approach. Instead, I fired off
these searches on DuckDuckGo and skimmed the results&mdash;but to no avail:

- "linux use ram"
- "simple way to use a ton of ram"
- "test high ram usage"
- "test high ram usage linux"

In retrospect, these definitely weren't _great_ queries, but I was having a hard time formulating something better.
These queries all gave me lots of hits for how to examine how much RAM is currently in use (with commands like `free`
and `top`), but they don't offer suggestions for how to actually _consume_ a ton of RAM.

It then occurred to me that perhaps I could just ask ChatGPT. Our conversation follows.

<blockquote>

**Me:** How can I test high RAM usage on a Linux computer?

**ChatGPT:** To test high RAM usage on a Linux computer, you can use the `free` command to check the amount of free and
used memory on the system. You can also use the `top` or `htop` commands to view real-time information about the
processes running on your system and their memory usage.

**Me:** Those commands show me how much memory is being used, but I want a command that will actually use a lot of
memory.

**ChatGPT:** To use a lot of memory on a Linux system, you can try using a command such as `dd` to allocate a large
amount of memory. For example, the following command will allocate 1GB of memory:

```
dd if=/dev/zero of=/dev/null bs=1G count=1
```

This command uses the `dd` utility to read from the `/dev/zero` device, which provides an infinite stream of zeros, and
writes it to the `/dev/null` device, which discards any data written to it. The `bs` option specifies the block size to
use, and the `count` option specifies the number of blocks to copy. In this case, the block size is 1GB and the count is
1, so the command will allocate 1GB of memory and use it to copy the stream of zeros.
</blockquote>

Wow!

As ChatGPT claims, that command does allocate a large chunk of memory. The only wrinkle is that the command completes a
little too quickly to be useful for my purposes, but that is easily solved by upping the `count` argument. I only have a
passing familiarity with `dd`, but as far as I understand it, ChatGPT's explanations of `dd` and `/dev/null`/`/dev/zero`
are completely correct.

But what I'm most impressed by is how natural the interface is: even to a knowledgeable human, I don't think my initial
question would have been 100% clear, and ChatGPT does indeed give me the same answers I already found online. The
difference here is that I can explain exactly what's wrong with the previous response in plain English, and get a
helpfully corrected answer&mdash;I absolutely cannot do that with a regular search engine!

After testing ChatGPT's answer, I improved my web search (with the query "linux command use lots of ram") and found two
relevant StackOverflow posts. The
[first](https://stackoverflow.com/questions/30018658/linux-program-command-to-intentionally-use-lots-of-ram) uses an
external utility, which I would like to avoid (although I didn't state this explicitly before). The
[second](https://stackoverflow.com/questions/4964799/write-a-bash-shell-script-that-consumes-a-constant-amount-of-ram-for-a-user-defi)
has some better information: the accepted answer captures the idea "allocate big objects in a script" (albeit in an
`ash` snippet I don't quite follow), and one of the comments also hits upon the `dd` solution.

This is far from the most impressive demonstration of ChatGPT's capabilities, but it's the one that convinced me:
keeping a ChatGPT tab open as a "search engine alternative" seems hard to dismiss now. (Although you do have to check
its work&mdash;I would _not_ recommend blindly running the first command it gives you&hellip;)
