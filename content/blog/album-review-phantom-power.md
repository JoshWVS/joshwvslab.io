+++
title = "Album review: <em>Phantom Power</em>"
date = 2025-01-05
+++

Over the past year or so, I've been focusing on owning my music collection (rather than streaming it from somewhere),
and being more deliberate about what I listen to. In that spirit, one overly-ambitious evening I invited a few friends
to participate in an "album review challenge:" we'd all pick one album that has a special significance to us, then put
together some kind of document explaining why we love that album. There weren't any restrictions on the format&mdash;my
wife put together a fantastic slideshow for hers; lacking her creativity, I confined myself to the written word.  Here's
my review of _Phantom Power_, by The Tragically Hip.

## Intro

My introduction to The Tragically Hip was their compilation album, _[Yer
Favourites](https://en.wikipedia.org/wiki/Yer_Favourites)_; one of many haphazard teenage HMV acquisitions. From there,
I started dipping into their discography proper, _Phantom Power_ included. Over a decade later, and over 25 years since
the album's release, I still find myself listening to it regularly&mdash;what gives it that staying power?

To answer that question, I'll go through the tracks one by one&mdash;some only get a few lines, others get a proper
write-up. I've also focused more on the lyrics than on the music itself; not because I think the former is more
important, but because I (sadly) lack the necessary vocabulary/understanding to comment meaningfully on the latter.  If
you don't own the album already, you can listen along on
[Spotify](https://open.spotify.com/album/3HWQXn3dGZPSnIhVkvHIOz?si=Y1HelLpoRPWPY1RpzEI2kw) or
[YouTube](https://www.youtube.com/watch?v=Aqzvs23FKiM&list=OLAK5uy_kNKJcyhMci8VN3WZiJs-VtQ69dMU5UgJw), and follow the
lyrics [here](http://www.hipmuseum.com/pplb.html).

One last thing before we dive in: in this whirlwind tour, I am greatly indebted to [A Museum After
Dark](http://www.hipmuseum.com/index.html), a shockingly comprehensive fan site dedicated to The Hip. In particular,
their page on [Phantom Power](http://www.hipmuseum.com/pplb.html) has been an immense resource in putting this together.
I link to their analysis in several places below; it has informed my writing in many other instances as well.

## Songs

### Poets

We're off to the races with a heavy hitter; this was the best-performing single from the album when it was first
released. It's also a quintessential example of Gord Downie's lyrical stylings: don't expect any easy answers or
simplistic imagery; Gord's going to smack you down with a sackful of obscure themes & allusions and see what sticks.

What have we got in this case? Some agricultural references ("Superfarmer's," "withered corn stalks," "long grasses over
time"), paired with sexual imagery for good measure ("porn speaks to its splintered legions", "lawn cut by bare-breasted
women"&mdash;note the latter might be a reference to Ontarian women getting [legal approval to go
topless](https://en.wikipedia.org/wiki/Female_toplessness_in_Canada#Gwen_Jacob) in 1996). I'm not quite sure what to
make of that particular combination ([Ceres](https://en.wikipedia.org/wiki/Ceres_(mythology)), maybe?), but I do want to
point out the implicit irony here: the song's narrator doesn't seem to have a very high opinion of "the poets," but Gord
is a pretty darn poetic guy himself&mdash;are we meant to be laughing at the poets, or at the narrator? "Himalayas of
the mind" indeed.

### Something On

In early 1998, Eastern Ontario and surrounding areas were hit with unprecedented quantities of freezing rain, causing
complete power outages for days, if not weeks. While most of us were hunkered down, waiting it out, The Hip were in the
studio recording this song ("outside there's hectic action / the ice is covering the trees").

One of my favourite details of this song is the restraint it shows in the chorus (i.e. the pairs of couplets that end
with "something/nothing on"). We hear it three times throughout the song, but the lyrics change each time, and our
eponymous epithet ("phantom power") only features once. By refusing to reuse the phrase, I feel it makes the climatic
moment before the bridge hit that much harder.

There's more to look at here that I don't have time to get into&mdash;we go from "space" tautening to "your face"
tautening; there's "impossible vacations" followed by "possible vacations", plus all the variations of "something" vs.
"nothing" being on. Further investigation will require some uninterrupted time with nothing to do&mdash;perhaps when I'm
invariably snowed in this winter. At least I'll have something to put on.

### Save the Planet

From the title you might expect a pretty straightforward "stop global warming" folk anthem, but things are never so
simple with The Hip. There's definitely themes of architecture ("granite," "bunkers," "minarets") and heavy machinery
("turbine roars," "tolerant hum from the core")&mdash;you might imagine they're setting up for a contrast against images
of nature, but that doesn't happen either. Instead, an eerily still man, covered in dust, repeatedly intones
"constitutions of granite can't save the planet." I hear that as a critique of the institutions of the post-WWII order:
maybe the forces that make efforts like the [Kyoto Protocol](https://en.wikipedia.org/wiki/Kyoto_Protocol) possible are
the same forces driving us to climate catastrophe.

But my favourite stanza is this synesthetic masterpiece:

> And it sounds heroincredible<br>
> Sound that makes the headphones edible<br>
> Awake affiliated and indelible

I have no idea what this means intellectually, and I can't find any other reference to "heroincredible," but I can still
_feel_ it, y'know?

### Bobcaygeon

This is my pick, far and away, for the most Canadian song ever.

Maybe there's some personal bias here, but who among us doesn't understand the exact feeling of watching the stars
overhead in small-town Ontario cottage country? Doesn't the gentle guitar melody at the start sound like something your
friend might strum by the campfire? And the narrator's romantic distraction&mdash;doesn't that too seem even more
Canadian when you learn that Gord sometimes introduced this song live as "[about a couple of gay cops that fall in
love](https://en.wikipedia.org/wiki/Bobcaygeon_(song)#cite_ref-2)?"

And yet, for all these charms, there's genuine danger lurking beneath the surface, both lyrically and in the musical
tension before the bridge. Many interpret the conflict in the song to be a reference to the [Christie Pits riot of
1933](https://www.thecanadianencyclopedia.ca/en/article/christie-pits-riot), which began with homemade banners featuring
swastikas and "Heil Hitler" flown at a baseball game featuring a Jewish team, and ended with over 10 000 participants
openly brawling in the streets. The police weren't able to quell the violence until the early hours of the morning.

Does this tell us something about ourselves? To me, this song comes across as fundamentally optimistic: yes, even here
in Canada we'll never be able to fully expunge humanity's darkest, basest prejudices. But maybe&mdash;just maybe&mdash;we can still build a kinder, gentler society; one where gay cops keep the peace and we all appreciate the constellations above. Bobcaygeon challenges us to try.

### Thompson Girl

We're going up waaay north for this next one: you probably caught that from the references to Churchill and polar bears,
but maybe you overlooked the "55 degrees"&mdash;that refers to the latitude; Thompson, Manitoba sits entirely above
55&deg; N. But the line that most catches my ear is "when she saw that _nickel stack_." Thompson's economy is largely
based on nickel mining, first developed by Inco Limited; Thompson probably has smokestacks of its own, but I'd be remiss
not to mention the [Inco Superstack](https://en.wikipedia.org/wiki/Inco_Superstack) in Sudbury&mdash;it's the
second-tallest freestanding structure in Canada, behind only the CN Tower (!!). It's so tall that after sending
pollutants up the stack, they generally just _float away from Sudbury on the wind_. Sure, I guess they'll eventually
fall as acid rain or whatever into the drinking water or something, but that's not Sudbury's problem.

But uh, back to the song&mdash;not much more to say on this one other than that it reminds of trying to make it through
a frigid Canadian winter and dreaming of the early green buds that herald spring.

### Membership

Look, I really don't have a blessed clue what's going on in this one. At first it vaguely sounds like it's about drugs;
later a bunch of ice fishing huts burn down off-screen? But if I'm being truly honest, I never really listen to the
lyrics on this one because I'm too busy singing my incredible stupid parody over the "chorus:"

_Show me some crap, I'll buy it_

_Sunday shopping; I got carried away_

_With my Costco membership_

### Fireworks
On an album with a wealth of instant classics, this is my personal favourite.

Thankfully, I had the good fortune of never playing hockey in my youth, but there was enough latent exposure in my
family for me to become intimately familiar with how all-consuming the hockey "lifestyle" can be for boys on the cusp of
adolescence. I love how it only takes two short stanzas for this track to evoke a poignant and quintessentially Canadian
meet-cute and coming-of-age story: after dreaming of nothing but the Cup from ages five to fifteen, all it takes is some
clammy hand-holding to realize that there might be more to life beyond the rink. It's so good, I'll quote the whole
thing below in case you don't have the lyrics handy:

<blockquote>
If there's a goal that everyone remembers it was back in ol '72<br>
We all squeezed the stick and we all pulled the trigger<br>
And all I remember is sitting beside you<br>

You said you didn't give a fuck about hockey<br>
I never saw someone say that before<br>
You held my hand and we walked home the long way<br>
You were loosening my grip on Bobby Orr
</blockquote>

That relationship sets up the rest of the song, which is thematically clearer and more consistent than other tracks
we've seen so far; this one examines how focusing on one's immediate family can give purpose and a sense of grounding
when the chaos of the world at large is too daunting to confront. I suppose I can't knock the wisdom of that approach
(of which I am, admittedly, an unintentional practitioner), but I also can't help but note some unspoken privilege in
that ambivalence: it's all well and good for you and me to stick our heads in the sand, but the people of, e.g., Ukraine
today don't have the same luxury&mdash;what obligations do we have to them? The song refers to the Cold War
specifically, but with geopolitical tensions ratcheting up seemingly daily, I can't help but feel the emotional core
here is becoming more and more relevant.

On a lighter note, there's a part of this song to which I can claim a familial connection: the seventh stanza references
the [Canada Fitness Award Program](https://en.wikipedia.org/wiki/Canada_Fitness_Award_Program), and in particular an
"eternal flexed arm hang" (which is exactly what it sounds like: lift yourself off the ground with a 90&deg; bend in
your arms and hold it as long as possible). Given the right occasion, my otherwise demure mother is wont to recount the
athletic accomplishments of her youth; she earned many a ribbon in her time, but her proudest such achievement was her
performance on that exact Canada Fitness Award Program flexed arm hang. She held it long enough to earn the Award of
Excellence, but that should go without saying&mdash;the real victory was that she held it _so_ long the gym teacher had
to ask her to stop, so that the other girls could give it a go as well.

After that, we finally we get to the titular fireworks, which our two lovers are (presumably) watching together. Maybe
this is crazy, but the only thing I can mentally picture here is a really wholesome version of the [final scene from
_Fight Club_](https://www.youtube.com/watch?v=nHKlfGq3bOA)&hellip;

Last but not least&mdash;if you haven't already, you really owe it to yourself to watch the [the goal of the
century](https://www.youtube.com/watch?v=lMf2fAXPS1Q) from the first line (and [read
up](https://en.wikipedia.org/wiki/Summit_Series) on how it connects to the Cold War themes).

### Vapour Trails

Next up we have what I consider to be the climax of the album, and perhaps its most underappreciated song. This one
feels _tense_ to me throughout, from the foreboding bass line at the start to the solo shredding near the end. The line
"there's nothing uglier than a man hitting his stride" feels especially apt to me&mdash;they hit a really dark and ugly
element there that I don't think is equalled elsewhere on the album.

This track is also resistant to coherent interpretation. My only quasi-sensible analysis of this one is an indictment of
how personal wealth can lead to carelessly inflicting negative externalities on others.

I know how crazy that sounds, but bear with me: you're [Tony Soparano](https://en.wikipedia.org/wiki/Tony_Soprano), or
[Kendall Roy](https://en.wikipedia.org/wiki/Kendall_Roy), or whoever. You're the narrator of this song: you have some
bullshit to take care of for work ("titillations been replaced by Interstate brickface and Coffee-Mate"), but
fundamentally, you're going to be fine ("go wherever you choose"). But you're ambitious&mdash;greedy, really&mdash;and
you're onto your latest scheme. Hopefully you'll hit pay dirt, but "&hellip;if it derails / you can throw away the
rudder and float away like vapour trails." (Maybe you're throwing away the rudder of the plane and giving up on landing
it safely&mdash;for most people that would be near-certain death, but _you_ can float away like vapour trails.)

Our narrator gets the beginning and ending of the song, but the middle is contrasted with the perspective of
agricultural workers: "Mexicans [&hellip;] in beige shirts" and "chambermaid's dark bare arms" are marching in sopping
wet shoes (while the narrator steps in "pools of light" and goes "flying home"). Is the narrator exploiting these people
by "giving them a ride?" Maybe, but it's not ironclad either&mdash;I'm perplexed by the mixed "you" vs. "he" pronouns in
the third stanza.

I don't see the narrator here as being intentionally _malicious_, just casually cruel. They're unabashedly trying to get
what they want: hopefully it goes well for everyone, but if not, the narrator will be the only one floating away on
vapour trails.

Was this song intended a critique of the growing inequality in Western societies? Probably not at all, no. But there's
something here that made me _think_: even if I'm only finding patterns in noise, that doesn't make it less interesting
to me.

Oh, one other thing: upon seeing the title _Vapour Trails_, my first thought was "like the conspiracy theory?"
Interestingly, [chemtrails](https://en.wikipedia.org/wiki/Chemtrail_conspiracy_theory) (the actual conspiracy theory)
date back to at least 1996, but they really only started picking up steam (ha) after getting radio coverage in 1999, one
year after this album was released. So is there an intended element of paranoia here? Hard to say.

### The Rules

This isn't a track I would reach for as a single, but I like it as part of the album&mdash;it gives a much-needed relief
to the tension of Vapour Trails. The lyrics aren't exactly positive, but the relaxed tempo and melodic motifs lighten
the mood. Sorry, no further analysis for this one; it's the rules.

### Chagrin Falls

I love the funky rift at the start of this one; it sounds a bit Eastern-inspired to my ears. I left the deep digging on
this one to [A Museum After Dark](http://www.hipmuseum.com/chagrin.html), which reports:

> Chagrin Falls is an actual village in Ohio, and is home to the massive corporate radio and entertainment conglomerate known as Clear Channel. The folks at CC have been accused of monopolistic business practices, curbing freedom of expression in the USA, and generally advancing the decline of American popular culture.

Also nowadays Ohio is [like a meme](https://knowyourmeme.com/memes/cultures/ohio) amongst the kids or something? Weird.

### Escape Is at Hand for the Travellin' Man

We're playing inside baseball and going a bit meta on this one: this is a band writing a song about a band playing and
seeing shows. According to Rob Baker (the lead guitarist), this song is about the fleeting encounters one has while
touring&mdash;people you instantly connect with on a deep level, but only share a single night with. I feel they capture
that well by contrasting the intimate ("you yelled in my ear this music speaks to me") with the indifferent ("it's not
like we were best of friends"). There's a strong sense of nostalgia and longing that comes through here musically as
well.

But my favourite part is the fake band/song names&mdash;I have to imagine The Hip had fun coming up with them. Quick,
which of these classic Escape is at Hand for the Travellin' Man hits is your fave?

1. _Lonely from Rock n' Roll_
2. _They Checked Out An Hour Ago_
3. _All Desired Turn Concrete_

### Emperor Penguin

We close out the album with something nice and easy to digest: an examination of the invisible labour of women, evoked
via the metaphor of emperor penguins (where, famously, the males sit around on the eggs, while the females go on an
arduous journey to fetch enough food to feed the entire family). A Museum after Dark covers it in more detail than I
will; if you want to go deeper, check out [their full analysis](http://www.hipmuseum.com/emporer.html). Instead, let me
make a few related points:

1. This is the second track on the album to describe sound as "edible" (see also [_Save The Planet_](#save-the-planet)).
2. Some fans claim the "alien invasion" stuff is a reference to an old radio show hosted by Art Bell. Remember how I
   told you that chemtrails got popular after getting radio coverage? Yep, that was from the same Art Bell show.
3. Finally, and most importantly, penguins aren't real.

"But Josh," you lamely protest, "I've _seen_ penguins. At the zoo."

No, you haven't. Our earliest record of the word "penguin" comes from the 16th century, where it's used as a synonym for
the great auk (_pinguinus impennis_). Great auks are black and white, flightless birds that live in the cold and are
excellent swimmers. Sound familiar? It shouldn't: we hunted them all to death (they lived in the Northern Hemisphere),
and they've been extinct since 1852.

As for those other _imposter_ birds you mistakenly labelled "penguins?" Brother, those are a group in the family
_Spheniscidae_, order _Sphenisciformes_: very similar in appearance and behaviour, and yet genetically very distant from
the greak auk. Yet another example of [convergent evolution](https://en.wikipedia.org/wiki/Convergent_evolution) in
action.

## Conclusion

Obviously, I love this album. But what is it that makes The Tragically Hip special? To me, there's a few key elements.

Firstly, their songs so often capture important Canadian stories in a way that feels authentic and effortless. On this
album alone we've seen the [1998 ice storm](https://en.wikipedia.org/wiki/January_1998_North_American_ice_storm), the
[Christie Pits Riots](https://www.thecanadianencyclopedia.ca/en/article/christie-pits-riot), and the [Summit
Series](https://en.wikipedia.org/wiki/Summit_Series), among many others&mdash;but none of those references are played
for a cheap "hey, remember this?;" they're always seamlessly integrated into a broader artistic narrative.

Secondly, many of these tracks are abstract poetry set to music; you can (and I have!) spend hours trying to pin them
down and dissect them, only to have a previously overlooked line catch you out of the blue and turn everything on its
head. You could dismiss these lyrics as self-indulgent, meaningless nonsense ("_[colorless green ideas sleep
furiously](https://en.wikipedia.org/wiki/Colorless_green_ideas_sleep_furiously)_"), but even if these were no more than
musical Rorschach tests, I don't think that would diminish their value: Gord didn't imbue these songs with some finite
amount of "artistic meaning" to be mined like a vein of minerals; rather that artistic meaning is _created_ every time
we choose to engage with these works. There's few bands more fun to mull over than The Hip.

Finally, there's the tragic element. Sometimes it feels that we as Canadians most revere those that were taken from us
too soon; in that regard, Gord rests comfortably among the likes of Jack Layton and Terry Fox. The Hip's final concert
will forever be recorded in the history of our nation itself; nearly a full third of our population watched it live. In
a sardonic twist of fate, Gord's own life has become a cherished Canadian story&mdash;one of those same sorts of stories
that The Tragically Hip shared with us so well. I find it hard not to feel that too when I listen to their music.

I've been challenged and delighted by these songs for years, and I can't see that changing any time soon. Thanks for
letting me share these [Himalayas of the mind](#poets) with you.
