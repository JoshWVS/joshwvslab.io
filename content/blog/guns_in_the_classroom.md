+++
title = "Guns in the classroom: a reprise"
date = 2020-08-30
+++

We are currently facing an education crisis in America. Our schools are underfunded, the unequal distribution of
resources reinforces disparities of race and class, and all the while our test scores continue to lag behind our
international peers'.  Now more than ever we need to prioritize education for everyone, and make our classrooms safe
places for learning. There is one natural solution: put a firearm into the hands of each and every teacher across the
nation.

First, the obvious reason: guns in schools ensure the physical security of our students. These days, our children face a
bevy of threats unimaginable since the cold war: deranged school shooters, militant ISIS sleeper cells, and radical
Antifa agitators, just to name a few. Arming our teachers would provide our children a vital first line of defence.
Taking a page from Silicon Valley, we could "gamify" training programs for teachers by tracking high scores and top
performers in the target range. Ideally, this program would be extended to the students themselves as well. This would
foster a sense of self-confidence and independence in our children (research-proven predictors of later success), while
also acquainting them with the military-industrial complex that makes our great country what it is today.

Second, guns in schools offer protection from COVID-19. Just as we sacrificed our grandmas for the sake of the
economy&mdash;a noble and virtuous choice&mdash;so too must we be willing to sacrifice young Jayden. If a student
displays telltale symptoms of COVID, their teacher could simply neutralize the snot-nosed threat before they begin to
spread the disease throughout the school. I am fully aware that some readers may be appalled by this suggestion, but it
is a gruesome necessity in these trying times. Consider the alternative: with these disease-infested gremlins slobbering
on each other all through playtime, COVID is sure to ravage our communities. What then? Parents staying home to tend to
their sickly families?  Who, then, will staff our meat-packing plants and Amazon warehouses? America has certainly
changed since its founding, but I think the Fathers of our Constitution would agree with this modern phrasing of our
country's core promise: each and every American, no matter race, colour, or creed, is entitled to cheap Chinese wares
with free two-day delivery. We are fundamentally a capitalist society, and we have fought for these ideals. To ask Jeff
Bezos to bear the costs of this pandemic would be a betrayal of those very same ideals.

Alas, there is an elephant in the room: COVID also means that virtually all instruction will be remote this fall.
Fortunately, just as the elephant gun solves the problem of the elephant, so too is there a firearm-based answer to this
conundrum. Consider the following: typical professional-grade drones can easily support a payload of 3-5 lbs., while
your standard Glock 19 is a mere 1.7 pounds fully loaded. Thus, we could simply assemble clusters of handgun-toting
drones to be operated by teachers. Not only does this solve the problems mentioned above, but it also allows for a safe
means of communication by which teachers could deliver their students personalized encouragement and/or hailstorms of
lead, as appropriate. (Say goodbye to late homework.) Think this is a massive cost for what will be a temporary online
session? Worry not. As school boards return to class, the gun-drones would simply be transferred to local police
departments. To continue effectively policing communities on reduced budgets, former beat cops could become drone
controllers, safely monitoring residents by operating dozens of gun-drones simultaneously. Looking ahead, advancements
in artificial intelligence could allow for autonomous operation of the drones, reducing costs even further. As a
God-fearing, law-abiding American, I don't have anything to hide, and I am confident the Amazon DroneNet&trade; will
agree.

Together, we can build a safer future for our children: one with an apple on every teacher's desk, and a pistol
in the drawer. God bless America.
