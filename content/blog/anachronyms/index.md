+++
title = "Anachronyms"
date = 2022-05-06
+++

Quick, what does KFC stand for?
<!-- more -->

<p style="margin-bottom:15em;"></p>

That's right, clearly they stand for nothing at all, because Colonel Sanders continues to perpetuate an unconscionable
chicken genocide on the order of billions of deaths per year in an unflinchingly single-minded pursuit of corporate
profits\![^1]

But while I certainly love delivering Col. Sanders a burn more severe than battering him (in both senses), then dunking
his head straight into a deep fryer, my goal today is not flaccid moral pontification&mdash;I have _linguistic_ aims in
mind.[^2]

You see, in a purely technical sense, KFC doesn't stand for anything either: in 1991, the restaurant chain hitherto
known as "Kentucky Fried Chicken" officially rebranded to "KFC."

This is known as an _anachronym_: a word that _used_ to be an acronym,[^3] but now exists in its own right, without
pointing to some other referent.

Except that isn't quite true either; it's probably more accurate to say that according to current "official" usage, you
could almost call KFC an _anacronym_ (without the "h"), but I have both semantic and orthographic bones to pick with
that&mdash;no Sanders, get that damn wishbone out of my face!

First of all, the meaning. The Oxford English Dictionary offers the following for _anacronym_: "an acronym or initialism
for which the expanded form is not widely known." Given that I rarely hear people talk about
light-amplification-by-stimulated-emission-of-radiation pointers, or self-contained-underwater-breathing-apparatus
diving, I think it's safe to count "laser" and "scuba" as examples. But going strictly by the OED definition, KFC is not
an anacronym&mdash;barring an episode of mass amnesia, everyone knows what the expanded form used to be. Regardless, KFC
fits the spirit of the word so perfectly, I couldn't imagine discounting it. Perhaps the definition could be amended to
"&hellip;for which the expanded form is not widely known _or officially used_."

Secondly, I feel a compulsion to sneak an extra "h" into the spelling (making it _anachronym_, not _anacronym_). I am
not alone in this wholesome habit; the enlightened editors of Network World also [endorsed this
spelling](https://web.archive.org/web/20220505194303/https://www.networkworld.com/article/2280053/acronyms--backronyms--and-anachronyms.html)
a mere&hellip; 14 years ago. The Online Etymology Dictionary gives the beginning of _acronym_ as the prefix
[acro-](https://www.etymonline.com/word/acro-), meaning "highest, topmost, _at the extremities_" (emphasis mine); I
suppose the letters of an acronym are indeed at the (frontal) extremity of their constituent words. Spelling it as
_anachronym_ destroys that semantic link, but I feel that is a worthy price to pay to instead bring in
[chrono-](https://www.etymonline.com/word/chrono-). The nod to Father Time is doubly appealing: usually it is his
relentless march that leads to the expanded form being lost (it becomes an ana-_chron_-ism!), and it would settle an
etymological matter. To the latter point, the Oxford English Dictionary thinks anacronym is "probably a blend of
anachronism n. and acronym n.," but also mentions the possibility that the first syllable is the prefix
_[an-](https://www.etymonline.com/word/an-)_, "hence with the meaning 'that is not an acronym'." Hogwash&mdash;this is
all about anachronisms and acronyms; let's put them front and centre.

So there you have it: a neat word to begin with, and doubly so if you join my rebel alliance of _anachronym_-ists, with
our corrected spelling and more inclusive definition. Since I'm rather taken with this as a corporate trend, let me
offer you two other Canadian anachronyms: that behemoth of boxed mac-and-cheese products was known for decades as "Kraft
Dinner" up north, but the suits [cut it
down](https://web.archive.org/web/20220429181435/https://www.cbc.ca/news/trending/kraft-dinner-officially-changes-its-name-to-kd-1.3174613)
to simply "KD" in 2015. Similarly, if you log in to the e-learning platform Brightspace, you're using a product from the
software company "D2L." Once upon a time, however, that was "Desire2Learn"&mdash;I don't have a press release to prove
this one, but the branded flip-flops I snagged during an internship there can attest to this important time in Canadian
history.

{{ image(
    src="./flipflop.jpg",
    alt='A photo of a flip-flop with the old Desire2Learn logo on the strap',
    caption="Ah, to be an intern again&hellip; subsiding solely on the contents of the snack cupboard (filled with Kraft Dinner, not KD, thank you very much!); nabbing any swag that wasn't nailed down&mdash;those were the days."
) }}

In fact, if you're going to grant me _anachronym_, we might as well go all the way. Let's define an _anarchonym_ (think
_anarchy_) to be "an acronym or initialism in which the order of the letters does not match that of the expanded form."
For instance, RABFU is an anarchonym: namely, it's a FUBAR "FUBAR." At this point we're only a stone's throw away from
_arachnonym_, which I guess is just any name for a pet spider? Fine, fine, I'll leave it there for now, I
promise&hellip;

<hr>

[^1]: A burn like that demands some kind of factual underpinning. Upon first writing that, my assumption was that all
    factory farms producing chickens are hellish abattoirs, designed simply to convert unadulterated misery into profit
    with maximum efficiency. I think perhaps this was informed by PETA's "Kentucky Fried Cruelty" campaign, which seems
    to date back to [at least 2003](https://web.archive.org/web/20030108103051/http://www.kentuckyfriedcruelty.com/); in
    particular, their [undercover
    footage](https://web.archive.org/web/20220506155056/https://www.peta.org/videos/tysons-dirty-deeds/) from a Tyson
    Foods slaughterhouse is heartbreaking. That being said, I was enthused to discover there are small rays of light
    beginning to pierce this bleak landscape.

The [Better Chicken Commitment](https://betterchickencommitment.com/) (BCC) outlines a set of policies for the more
humane treatment of chickens in the food industry;[^4] over the past few years a number of big names have pledged to
meet these standards. [World Animal Protection](https://www.worldanimalprotection.org/) tracks companies' progress
towards these targets in their annual report, _The Pecking Order_ (yes, really). The [2021
edition](https://web.archive.org/web/20220505153208/https://www.worldanimalprotection.org/pecking-order-2021) is a mixed
bucket for KFC: according to the report, most national operations follow Yum! Brands' [global animal welfare
policy](https://web.archive.org/web/20210907025518/https://www.yum.com/wps/wcm/connect/yumbrands/4c34c593-4766-446d-9132-fe53ea18d3e8/Global-Animal-Welfare-Policy-Formatting-V3-082621.pdf?MOD=AJPERES&CVID=nKEfHIP)
(note the general lack of hard targets&hellip;), which "explicitly covers chicken welfare, but is not aligned with the
BCC;" those countries (including the US) score abysmally (typically 5 out of 90 points). However, some of the European
operations fare much better. KFC UK & Ireland is the star among them, achieving the top rank of "Tier 1 - Leading" after
earning the full 30 marks for "corporate commitments" as well as "objectives and targets," and a further 23/30 on
"performance reporting." They earned points in that latter category for their annual chicken welfare report. Those
reports are fascinating, but you'd be forgiven for not finding them&mdash;at time of writing, any request I make to
[www.kfc.co.uk](https://www.kfc.co.uk) is being denied (403 Forbidden). As usual, the [Wayback
Machine](https://web.archive.org/) comes to the rescue; here's [the 2021
edition](https://web.archive.org/web/20211024020503/https://brand-uk.assets.kfc.co.uk/KFC_Annual_Progress_3007_UK-v2.pdf)
(see also [the 2020
edition](https://web.archive.org/web/20220505155534/http://kfc-uk-brand.s3.eu-west-1.amazonaws.com/drupal/production/2020-07/KFC_Annual_Report_Chicken_Welfare_2020.pdf),
which has a very helpful high-level diagram of the supply chain).

Let's be clear: this is not enough. According to KFC's own report, several metrics worsened in 2020&mdash;e.g.,
mortality hitting its highest recorded level (4.37%, up from 4.00% in 2019), a lower percentage of high welfare breeds
were in use (1.73%, down from 2.65%&mdash;if I'm reading the BCC correctly, they've pledged to get this to 100% by
2026&#x203d;), and a bump in executions by electric water bath (generally considered to be less humane than "controlled
atmosphere stunning" (CAS); CAS accounted for 62% of deaths in 2019, but only 57% in 2020). It seems we're also still in
the (probably unavoidable) "cheap talk" stage of the BCC: earning the full 30 marks for "corporate commitments" is nice,
but at some point you do actually have to _make things better_. (What's more, all of this already assumes that the best
option&mdash;just not eating the chickens in the first place&mdash;is not even on the table.)

However, viciously criticizing KFC UK & Ireland using the welfare data they willingly publish would be tantamount to
[drunkenly stumbling under the streetlight](https://en.wikipedia.org/wiki/Streetlight_effect). Assuming this data is
being reported accurately,[^5] KFC UK & Ireland is providing a level of transparency far beyond any of their
competitors; while these staid corporate charts belie an ocean of suffering, they are also an invaluable tool in
alleviating that suffering. What we can measure, we can improve; while we must remain mindful of [Goodhart's
Law](https://en.wikipedia.org/wiki/Goodhart%27s_law), I remain confident that a solid empirical grounding provides our
best chance at effecting meaningful change.

So while I still feel justified in my snarky barb, KFC UK & Ireland earns my kudos as well. Once again, _this isn't
enough_, but it is a commendable start&mdash;and that deserves to be recognized.

[^2]: Well, perhaps _had_ is more accurate; the chicken welfare digression ended up overwhelming the rest.

[^3]: Yes, yes, KFC was an initialism, not an acronym; gold star for you; it's now an anachronym all the same.

[^4]: Yes, this is a footnote for a footnote, and yes obviously it isn't typeset correctly; I'm not happy about it
    either. More to the point, I find it curious that the Better Chicken Commitment website offers no information on the
    history of the project&mdash;when it started, who the original sponsors were, etc. It's only from a [PDF copy of a
    letter](https://web.archive.org/web/20220505144212/https://betterchickencommitment.com/letter.pdf) at the bottom of
    their policy page that we find a date (November 2019), and a list of signatories (The Humane League, Animal
    Equality, The Humane Society of the United States, Mercy for Animals, World Animal Protection, Canadian Coalition
    for Farm Animals, Humane Society International Canada, American Society for the Prevention of Cruelty to Animals,
    Compassion in World Farming, and Four Paws). I'm also surprised that while (version two) of the BCC lays out
    conditions on stocking density, floor covering, "functional enrichments," acceptable slaughter methods, and more,
    there's no mention of debeaking. Per
    [Wikipedia](https://web.archive.org/web/20220505151454/https://en.wikipedia.org/wiki/Debeaking#Legislation), some
    European countries have begun to outlaw debeaking and "analysts expect the practice to be gradually banned across
    the continent"&mdash;to me, that suggests there must be some anti-debeaking momentum; its absence from the BCC
    strikes me as slightly unusual. (This might just be my own ignorance on display; I am not a subject matter expert
    here, and may be overlooking an obvious explanation.)

[^5]: I don't have much direct evidence that would lead me to doubt this, but I'm starting from a place of vague general
    distrust born of reading one too many "oops, giant multinational did a greenwashing!" articles. In that vein, see
    [this video](https://web.archive.org/web/20220505232216/https://vfcfoods.com/where-does-kfc-chicken-come-from/) from
    VFC Foods, which claims that some of KFC's "Behind the Bucket" marketing is at best puffery, and potentially
    downright misleading. I do think the report figures are more likely to be accurate than KFC's marketing, and it's
    worth noting that VFC does offer a competing vegan product, but still&hellip; this doesn't improve my perception of
    KFC.
