+++
title = "Life in a tandemic"
date = 2022-04-27
+++


It seems that biking across the Golden Gate Bridge is perhaps _the_ quintessential San Francisco tourist activity.

For the life of me, I cannot understand why.

<!-- more -->

Here's the situation: I've accompanied my brother (yes, [that brother](@/blog/the_two_tractor_challenge/index.md)) on a
business trip to San Francisco. Unfortunately, he immediately falls victim to el Corona and spends most of the trip
taking a detailed inventory of the stucco stalactites on his hotel room's ceiling. By the last day of the trip, he's
feeling better, completed his CDC-recommended isolation, and eager to salvage what fun he can in the time left. Me, the
big softie that I am, don't have the heart to deny him.

"So, what do you want to do?"

"I want to go biking."

"Sure, sounds cool. We can rent a couple bikes and explore the city a bit."

"No no, _tandem_ biking."

(pause, sigh) "Okay."

As it turns out, if you want a tandem bike rental on short notice, you're going to have to go somewhere that specializes
in tourists. We drop our bags at the front desk, and hoof it over to Blazing Saddles in Fisherman's Wharf.

The atmosphere there is friendly, if not overly "professional"&mdash;I get a general sense that the employees are
counting down the minutes until they can hit up happy hour and subsequently do unprintable things to each other. We're
breezily passed from one employee to another in an invisible, yet extremely high-speed conveyor belt that sees us
checked in, subject to a truly baffling instructional video, fitted with helmets, and finally taken to the basement to
retrieve our four-pedaled chariot.

As best I can tell, basement guy's job is to nominally ensure that you can actually operate the equipment you've
rented. I say nominally because clearly we cannot&mdash;at first, we succeed only in moving the bike sideways as we tip
it over repeatedly. When we progress to forward momentum, it's in such a drunken, stumbling lurch that we would have
summarily been issued a DUI, were we capable of making it more than ten metres ahead at a time. This is also when I
learn that my cycling enthusiast brother has never ridden a tandem either. My confidence levels&mdash;not high to begin
with&mdash;are shot, and I express to my brother that perhaps we'd be better off switching to separate bikes after all.

Basement guy isn't having any of it. "You'll get the hang of it," he insists. "Just do me a favour and _walk_ the bike
down this hill before you start riding it, okay? Have fun!" I manage to request a map before we're frogmarched out the
front.

Mercifully, we begin cycling on a separated bike lane, and do indeed get the hang of it fairly quickly. (Turns out it's
pretty important to turn the pedals _at the same time_&mdash;a rather astonishingly obvious nugget of advice, but one
that was never forthcoming from basement guy.) When you're on a rental bike on the north shore of SF, there's only one
destination, and so we set our sights on the Golden Gate Bridge.

My brother's riding up front, as I had less than zero desire to navigate, which leaves me in the back. The rear seat,
I'm told, is responsible for generating more of the "power" to propel us forward. When I first made the choice, that
seemed a perfectly reasonable compromise, but as we approach the first steep hill, I begin to have second thoughts. My
brother insists the bike's frame is made of steel, but I could swear it's filled with lead (an anti-theft
measure??). Speaking of my brother, he's no light load either&mdash;a fit guy, certainly, but it doesn't take us long to
realize that having recently had one's lungs ravaged by COVID somewhat puts a damper on one's cardiovascular
capabilities. Combined, these factors conspire to make even the slightest incline a Sisyphean task&mdash;well, perhaps
more accurately a _Herculean_ task, because unlike that other arrogant ninny, I eventually _succeed_ at rolling our
asses up that hill (with only a minimum of wheezing, gasping, and panting). The bridge is before us!

As we approach, we learn that only one side of the bridge is open to non-motorized traffic, and it's not particularly
wide&mdash;were I to stand in the middle and fully stretch out my arms, I don't think I'd quite reach both sides, but it
would be close. (Of course, actually attempting this experience would have spelled certain death by trampling, so please
accept my approximation.)

Inexplicably, the open side of the bridge also happens to be under construction. For those keeping score at home, that
leaves one half of one (narrow! did I mention narrow&#x203d;) path to be shared by both pedestrians and cyclists,
travelling in both directions, all while avoiding the construction crews _and vehicles_ working on the bridge.

I see no construction activity of any kind on the other (closed, empty) side of the bridge.

So, this is what it's like to _actually_ ride across the Golden Gate bridge: your eyes fix upon the nearest family
lumbering towards you. Their perfect linear formation would be the envy of any 19<sup>th</sup>-century infantry company, and they
occupy the full width of the path. Your manic cries and gesticulations fall on deaf ears; they cannot be swayed. You
grit your teeth, find your opening, and pray that the father's corpulence will cushion his young daughter should a
collision become unavoidable.

Now to be fair to the legions of zombies crossing the bridge, it _is_ quite difficult to hear anything over the enormous
din of traffic rushing by a foot to your left and the power tools being operated a foot to your right (not to mention
the gale-force winds buffeting you from every direction). Even if you could hear just fine, your cognitive functions and
reaction speeds are heavily impaired by the (literally) intoxicating bouquet of emissions surrounding you&mdash;the
rich, hearty medley of car exhaust complemented quite beautifully by a hint of sweetness from the two-stroke diesel
fumes.

Affixed at regular intervals are suicide prevention signs. I had assumed that the bridge, with her morose glory and
imposing height, was often selected as the final, premeditated destination for those lost to their depression, but in my
current situation I empathize with anyone simply desiring to make an unplanned and expedited exit.

The reward for your harrowing crossing is Vista Point. Would you like to know what it looks like from Vista Point? It
looks like this:

{{ image(
    src="./vista_point_view.jpg",
    alt="A photograph of the view from Vista Point",
    caption='"[Vista Point - San Francisco, California](https://flickr.com/photos/dougtone/7363984986)," by [Doug Kerr](https://flickr.com/photos/dougtone), is licensed under [CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/). I somehow forgot to take my own photo.'
) }}


It's fine! It's nice! You and the hundreds of other tourists can stand there, cheek-to-cheek, pretend not to notice each
other, and look at the city. It's pretty.

There's a comfortably-sized parking lot here, and while I would usually rue the banality of erecting an asphalt
ellipsoid for the sole purpose of allowing people to _drive here_ and then _look at where they came from_, on this
afternoon I simply can't find it in me to muster up a smug, self-satisfied grin while surveying the cavalcade of
Cadillacs. At least they didn't have to inhale their own exhaust on the way over.

The way back across the bridge is no different, and certainly no better. (I suppose this is why most tourists opt to
continue exploring to Sausalito and take the ferry back from there&mdash;quitters, I say, and out an extra thirteen
clams per head!) Fortunately, we have a new destination upon which to pin our touristic hopes and dreams: Golden Gate
Park. Now Golden Gate Park is one of those baffling, uniquely _American_ creations. To reserve an expanse of a
rapidly-growing city and declare:

> This land shall be a respite for the _public_&mdash;upon these borders, the corrosive industry of urbanism shall never
> intrude. But preserving nature need not imply an embrace of raw wilderness either; rather, this land shall be dotted
> with all that which makes this life worth living. It shall be replete with both botany and science; art and athletics;
> music and Zen; and, of course, natural terrain of all kinds.

That is a thing of beauty indeed. If we are capable of building a utopia, it will be in the spirit of Golden Gate Park.

But it won't be _exactly_ like Golden Gate Park, because, for all its heart-stirring, high-minded idealism, the actual
park is over a thousand acres in area&mdash;if you show up on foot, you aren't getting far. This nature preserve is so
expansive that they had to pave roads right through it; it's so grand that your recommended options for taking it all in
are shuttle bus or Segway tour. All of this transpires without a perceptible hint of irony. On our bike we fare just
fine, managing to squeezing in some lovely sights while only needing to navigate a handful of high-speed intersections.

Having taken in some of the best that San Francisco has to offer, we had only to return that infernal tandem bike. But,
like any good curse, we could not be rid of it without a final trial&mdash;retracing our relatively safe, bike-friendly
path would take far too long; we were going to have to cut through the heart of the city itself.

If, like me, your cycling experience is limited to an occasional Sunday saunter, you might not be overly familiar with
the finer points of shifting gears. Here's the challenge we faced: most of our route involved going straight along a
road uphill with frequent intersections. When the light turns green, you need to be in a low gear to afford yourself
some chance of inspiring the tandem to motion from standstill. Soon after though, you'll need to gear up again;
otherwise your feet will be flying while the bike barely moves, and adjacent motorists will gladly risk your lives if it
means getting around you. So, fine, start low and then gear up&mdash;but, _but_, and this is easy to miss&mdash;when
you're near the top of the hill, you need to pre-emptively gear down again, because you almost certainly aren't making
that light, and if you don't gear down now, you'll come to a stop in a high gear, so when you need to start moving
again&mdash;game over. If that makes no sense to you, pay no heed to it. All you need to know is that during our trial
by fire, we, unsteady freshmen, had graduated and become perfectly synchronized co-pilots, calmly reading instruments,
exchanging commands, and making adjustments as needed. Our steed was not quite the
[SR-71](https://web.archive.org/web/20220427214925/https://www.thesr71blackbird.com/Aircraft/Stories/sr-71-blackbird-speed-check-story),
but the professionalism in our cockpit was no lesser.

As I have previously noted the physical toll of operating the rear end of a tandem bicycle, I am obligated to also point
out the accompanying mental lightness. Typically, navigating hostile vehicular traffic amid unfamiliar terrain would
provoke a certain wide-eyed, white-knuckle terror in me. On this occasion, however, my view was almost entirely occluded
by my brother's brutish back, and losing sight of the myriad dangers surrounding us inspired a somewhat blissful
equanimity as to my own fate. _"If we die, we die,"_ I shrugged mentally, _"my job is to pedal."_ And pedal I did.

Somehow, miraculously, we made it back&mdash;a small litany of traffic violations to our names, perhaps, but
uninjured. Well, uninjured except for one thing. I didn't think to put sunscreen on my arms and burnt them to a
crisp. Sitting at home now, I watch the skin slough off my forearm and gently sway in the air&mdash;as if it were still
caught on a sunny Pacific breeze.

San Francisco is a wonderful, beautiful, impossible place. If you're able to visit, I highly recommend you do so. Just
skip the tandem bike, and hike up Twin Peaks instead.
