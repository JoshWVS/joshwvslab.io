+++
template = "generic_content.html"
+++

# Projects

Isn't it fun to get computers to do things for you? Here are some of my favorite things that I got computers to do for
me.

{% project(
    title = "Emoji Summarizer",
    status = "complete",
    source_url = "https://gitlab.com/JoshWVS/emoji-summarizer",
    link = "https://emoji-summarizer.simpsonian.ca/"
) %}

An offhand comment from my brother sparked a one-day personal "hackathon" to
call forth the [masked
shoggoth](https://web.archive.org/web/20230618205227/https://knowyourmeme.com/memes/shoggoth-with-smiley-face-artificial-intelligence)
for myself. Emoji Summarizer (yes, I am bad at naming things) converts your
boring, squiggly, abstract "words" to the one universal language: emoji.

Technical details: trivial Flask app using the OpenAI API on the backend;
[htmx](https://htmx.org/) for the frontend.
{% end %}

{% project(
    title = "ToyRSS",
    status = "in development",
    source_url = "https://gitlab.com/JoshWVS/toyrss",
    link = "https://toyrss.simpsonian.ca/"
) %}

My first experience using a feed reader was when I started self-hosting [Miniflux](https://miniflux.app/) in 2020. I
can't say enough good things about it; Miniflux is a wonderful piece of software that has really changed how I consume
digital media, and sparked a personal interest in activity feeds. I started writing ToyRSS as part of that exploration:
I had a general high-level picture of how a feed reader could work, but I wanted to build one myself.

Technical details: ToyRSS is a web-based feed reader written in Rust. It uses [axum](https://docs.rs/axum/latest/axum/)
for HTTP routing, [Tera](https://docs.rs/tera/latest/tera/) for templating, [htmx](https://htmx.org/) for improved
interactivity, and [SQLite](https://www.sqlite.org/index.html) for storage.
{% end %}

{% project(
    title = "Asteria",
    status = "in development",
    source_url = "https://gitlab.com/JoshWVS/asteria",
    link = "https://gitlab.com/JoshWVS/asteria"
) %}

My most recent PC build (December 2022) was the first time I've had fancy-schmancy LEDs on my computer case. Pulsating
rainbow patterns are cool at first, but for me, the novelty wore off quickly&mdash;I wanted _more_; I wanted to make
those LEDs _useful_. Asteria is my attempt to do so: now I've got my GPU temperature, CPU temperature, and memory usage
all visualized on the front of my case.

Technical details: Asteria is a simple driver for the [OpenRGB](https://openrgb.org/) [Python
SDK](https://openrgb-python.readthedocs.io/en/latest/pages/usage.html). It provides modular components to facilitate
configuring interesting and useful LED effects.

{% end %}

{% project(
    title = "Chaos Calculator",
    status = "backburner",
    source_url = "https://gitlab.com/JoshWVS/chaos-calculator",
    link = "https://gitlab.com/JoshWVS/chaos-calculator"
) %}

Of the many board games I've played, precious few have worked their way into my bloodstream so deeply as [Arkham Horror:
the Card Game](https://boardgamegeek.com/boardgame/205637/arkham-horror-card-game). It's a game of teamwork and of
roleplaying; of facing down impossible cosmic monstrosities with nothing but a steely glint in your eye and a
[candlestick](https://arkhamdb.com/card/02029) in your hand.  And if you're like me, it's also a game about obssessing
over miniscule cost-benefit analyses to a degree that would make an economist blush. Chaos Calculator is a tool to
assist with that hand-wringing. Most of the variance in the game comes from "skill tests" the players have to perform;
Chaos Calculator runs thousands of simulations of those tests and reports some relevant probabilities in graphical and
tabular form.

Technical details: this is an [R Shiny](https://shiny.rstudio.com/) app, which is my favorite framework for quickly
building highly functional data exploration tools. Yes, the user experience would probably be improved if this whole
thing was written as client-side JavaScript&mdash;but when R already has a built-in for everything I need, why wrestle
with that pig?
{% end %}

{% project(
    title = "MOTD Poetry",
    status = "complete",
    source_url = "https://gitlab.com/JoshWVS/motd-poetry",
    link = "https://gitlab.com/JoshWVS/motd-poetry"
) %}

Like many people, I would often _say_ that I'd like to read more poetry, then make absolutely zero effort to do so.
Well, here's my two-step solution to fix that:

1. Write a trivial Python script to fetch the Poetry Foundation's [Poem of the
   Day](https://www.poetryfoundation.org/poems/poem-of-the-day).
2. Set up a daily cron job to populate `/etc/motd` with that script's output.

Now I'm routinely exposed to poetry whether I like it or not.
{% end %}
